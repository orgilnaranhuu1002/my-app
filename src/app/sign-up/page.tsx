'use client'
import React, { Suspense, useEffect, useState } from 'react'
import SignUp from '../components/User/pages/SignUp'
import MiniSignUp from '../components/User/pages/MiniSignUp';
import Loading from "../components/Loader"
import NextNProgress from 'nextjs-progressbar';


const SignUpPage = () => {
	const [isMobile, setIsMobile] = useState(true);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth >= 900)
    };

    handleResize(); // Check initial screen width
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return <>
  <NextNProgress />
	{isMobile ? <SignUp />: <MiniSignUp/>}
 
	</>
}

export default SignUpPage