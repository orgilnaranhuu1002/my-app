import type { NextApiRequest, NextApiResponse } from 'next';
import * as nextConnect from 'next-connect';
import multer from 'multer';
import cloudinary from '../../../dbConfig/cloudinary';
import { unlink } from 'fs/promises';

const upload = multer({ dest: '/tmp' });

const apiRoute = (nextConnect as any)({
  onError(error: any, req: NextApiRequest, res: NextApiResponse) {
    res.status(501).json({ error: `Something went wrong! ${error.message}` });
  },
  onNoMatch(req: NextApiRequest, res: NextApiResponse) {
    res.status(405).json({ error: `Method '${req.method}' Not Allowed` });
  },
});

apiRoute.use(upload.single('profileImage'));

apiRoute.post(async (req: any, res: NextApiResponse) => {
  try {
    const file = req.file;
    const uploadResult = await cloudinary.uploader.upload(file.path, {
      folder: 'profile_pics',
    });

    await unlink(file.path); // Delete the file from the server

    res.status(200).json({ imageUrl: uploadResult.secure_url });
  } catch (error) {
    res.status(500).json({ error: 'Failed to upload image' });
  }
});

export const config = {
  api: {
    bodyParser: false, // Disallow body parsing, since we're using multer
  },
};

export default apiRoute;