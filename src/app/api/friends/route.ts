import { connectToMongoDB } from '@/dbConfig/dbconfig';
import User from '@/models/userModel';
import { NextRequest, NextResponse } from 'next/server';

export async function POST(request: NextRequest) {
    try {
        await connectToMongoDB();

        const { userId, friendId } = await request.json();
        const user = await User.findById(userId);
        if (!user) {
            return NextResponse.json({ error: 'User not found' }, { status: 404 });
        }

        const friend = await User.findById(friendId);
        if (!friend) {
            return NextResponse.json({ error: 'Friend not found' }, { status: 404 });
        }

        if (user.friends.includes(friendId)) {
            return NextResponse.json({ error: 'Friend already added' }, { status: 400 });
        }

        user.friends.push(friendId);
        await user.save();

        return NextResponse.json({ message: 'Friend added successfully' }, { status: 201 });
    } catch (error) {
        console.error('Error adding friend:', error);
        return NextResponse.json({ error: 'Internal Server Error' }, { status: 500 });
    }
}

export async function DELETE(request: NextRequest) {
    try {
        await connectToMongoDB();

        const { userId, friendId } = await request.json();

        const user = await User.findById(userId);
        if (!user) {
            return NextResponse.json({ error: 'User not found' }, { status: 404 });
        }

        if (!user.friends.includes(friendId)) {
            return NextResponse.json({ error: 'Friend not found in user\'s friend list' }, { status: 404 });
        }

        user.friends = user.friends.filter((id:string) => id !== friendId);
        await user.save();

        return NextResponse.json({ message: 'Friend removed successfully' }, { status: 200 });
    } catch (error) {
        console.error('Error removing friend:', error);
        return NextResponse.json({ error: 'Internal Server Error' }, { status: 500 });
    }
}
