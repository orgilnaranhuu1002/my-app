import { connectToMongoDB } from "@/dbConfig/dbconfig";
import User from "@/models/userModel";
import { NextResponse, NextRequest } from "next/server";
import { ObjectId } from "mongodb";
import bcrypt from "bcryptjs";
export async function PUT(request: NextRequest, { params }: { params: { id: string } }) {
    const { id } = params;
    try {
        const { username, email, newPassword, profileImage } = await request.json();
        await connectToMongoDB();

        const updateData: any = { username, email, profileImage };

        if (newPassword) {
            const salt = await bcrypt.genSalt(10);
            const hashedPassword = await bcrypt.hash(newPassword, salt);
            updateData.password = hashedPassword;
        }

        const updatedUser = await User.findByIdAndUpdate(
            new ObjectId(id),
            updateData,
            { new: true, runValidators: true }
        );

        if (!updatedUser) {
            return new Response("Failed to update user", { status: 400 });
        }

        return new Response(JSON.stringify(updatedUser), {
            status: 200,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    } catch (error) {
        return new Response(JSON.stringify({ error }), {
            status: 500,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
}


export async function DELETE(request: NextRequest, { params }: { params: { id: string } }) {
    const { id } = params;
    try {
        const { password } = await request.json();
        await connectToMongoDB();

        const user = await User.findById(id);
        if (!user) {
            return new Response("User not found", { status: 404 });
        }

        // Verify password (ensure to compare hashed passwords)
        const isMatch = await user.comparePassword(password);
        if (!isMatch) {
            return new Response("Incorrect password", { status: 401 });
        }

        await User.findByIdAndDelete(new ObjectId(id));
        return new Response(JSON.stringify({ message: "User deleted" }), {
            status: 200,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    } catch (error) {
        return new Response(JSON.stringify({ error }), {
            status: 500,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
}

export async function POST(request: NextRequest) {
    const { userId, password } = await request.json();

    if (!userId || !password) {
        return new Response(JSON.stringify({ message: "User ID and password are required" }), { status: 400, headers: { 'Content-Type': 'application/json' } });
    }

    try {
        await connectToMongoDB();
        const user = await User.findById(userId);

        if (!user) {
            return new Response(JSON.stringify({ message: "User not found" }), { status: 404, headers: { 'Content-Type': 'application/json' } });
        }

        const isPasswordValid = await bcrypt.compare(password, user.password);

        if (!isPasswordValid) {
            return new Response(JSON.stringify({ message: "Invalid password" }), { status: 401, headers: { 'Content-Type': 'application/json' } });
        }

        return new Response(JSON.stringify({ message: "Password verified" }), { status: 200, headers: { 'Content-Type': 'application/json' } });
    } catch (error) {
        console.error("Error verifying password:", error);
        return new Response(JSON.stringify({ message: "Error verifying password" }), { status: 500, headers: { 'Content-Type': 'application/json' } });
    }
}