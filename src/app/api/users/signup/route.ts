import { connectToMongoDB } from '@/dbConfig/dbconfig';
import User from '@/models/userModel';
import { NextRequest, NextResponse } from 'next/server';
import bcryptjs from 'bcryptjs';

export async function POST(request: NextRequest) {
    try {
        console.log("Connecting to MongoDB...");
        await connectToMongoDB();
        console.log("Connected to MongoDB.");

        const reqBody = await request.json();
        const { username, email, password, profileImage } = reqBody;
        console.log("Request body parsed:", reqBody);

        if (!username || !email || !password) {
            return NextResponse.json(
                { error: 'Missing fields in request' },
                { status: 400 }
            );
        }

        const existingUser = await User.findOne({ email });
        if (existingUser) {
            return NextResponse.json(
                { error: 'This user already exists' },
                { status: 400 }
            );
        }

        const salt = await bcryptjs.genSalt(10);
        const hashedPassword = await bcryptjs.hash(password, salt);

        const newUser = new User({
            username,
            email,
            password: hashedPassword,
            profileImage: profileImage || ''
        });

        console.log("Saving new user:", newUser);
        const savedUser = await newUser.save();

        return NextResponse.json({
            message: 'User created!',
            success: true,
            user: {
                _id: savedUser._id,
                username: savedUser.username,
                email: savedUser.email,
                profileImage: savedUser.profileImage
            },
        }, { status: 201 });
    } catch (error) {
        console.error('Error during signup:', error);

        return NextResponse.json(
            { error: 'Internal Server Error' },
            { status: 500 }
        );
    }
}

export async function GET(request: NextRequest) {
  try {
    await connectToMongoDB();

    const token = request.cookies.get('token')?.value;
    if (!token) {
      return NextResponse.json({ message: "Authentication required" }, { status: 401 });
    }

    
    const users = await User.find({});
    return NextResponse.json({ users }, { status: 200 });

  } catch (error) {
    console.error("Error retrieving users:", error);
    return NextResponse.json({ message: "Error retrieving users" }, { status: 500 });
  }
}