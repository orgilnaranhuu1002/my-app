import { connectToMongoDB } from '@/dbConfig/dbconfig';
import User from '@/models/userModel';
import { NextRequest, NextResponse } from 'next/server';
import bcryptjs from 'bcryptjs';
import jwt from 'jsonwebtoken';

connectToMongoDB();

export async function POST(request: NextRequest) {
  try {
    const reqBody = await request.json();
    const { email, password } = reqBody;
    console.log('Request Body:', reqBody);

    const user = await User.findOne({ email });
    if (!user) {
      console.log('User not found');
      return NextResponse.json(
        { error: 'User does not exist in DB' },
        { status: 400 }
      );
    }

    console.log('User found:', user);

    const validPassword = await bcryptjs.compare(password, user.password);
    console.log('Password match:', validPassword); // Log the result of password comparison
    if (!validPassword) {
      console.log('Invalid password');
	  console.log('Plain Password:',validPassword);
      return NextResponse.json({ error: 'Invalid password' }, { status: 400 });
    }

    const tokenData = {
      _id: user._id,
      username: user.username,
      email: user.email,
      profileImage: user.profileImage,
    };

    const token = await jwt.sign(tokenData, process.env.TOKEN_SECRET!, {
      expiresIn: '2d',
    });

    const response = NextResponse.json({
      message: 'Login Successful',
      success: true,
      user: {
        _id: user._id,
        username: user.username,
        email: user.email,
      },
    });

    response.cookies.set('token', token, { httpOnly: true });
    return response;
  } catch (error: any) {
    console.error('Error during login:', error);
    return NextResponse.json({ error: error.message }, { status: 500 });
  }
}
