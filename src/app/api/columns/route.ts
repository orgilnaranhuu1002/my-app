import { connectToMongoDB } from "@/dbConfig/dbconfig";
import Column from "@/models/column";
import { NextResponse, NextRequest } from "next/server";
import jwt, { JwtPayload } from 'jsonwebtoken';

export async function POST(request: NextRequest) {
  try {
    await connectToMongoDB();

    const token = request.cookies.get('token')?.value;
    if (!token) {
      return NextResponse.json({ message: "Authentication token is missing" }, { status: 401 });
    }

    let userIdFromAuth;
    try {
      const decoded = jwt.verify(token, process.env.TOKEN_SECRET!) as JwtPayload;
      userIdFromAuth = decoded._id;
    } catch (error) {
      return NextResponse.json({ message: "Invalid or expired token" }, { status: 401 });
    }

    const { name } = await request.json();
    const newColumn = await Column.create({ name, userId: userIdFromAuth });

    return NextResponse.json({ message: "Column Created", column: newColumn }, { status: 201 });
  } catch (error) {
    console.error("Error creating column:", error);
    return NextResponse.json({ message: "Error creating column" }, { status: 500 });
  }
}

export async function GET(request: NextRequest) {
  try {
    await connectToMongoDB();

    const token = request.cookies.get('token')?.value;

    if (!token) {
      return NextResponse.json({ message: "Authentication token is missing" }, { status: 401 });
    }

    let userIdFromAuth;
    try {
      const decoded = jwt.verify(token, process.env.TOKEN_SECRET!) as JwtPayload;
      userIdFromAuth = decoded._id;
    } catch (error) {
      return NextResponse.json({ message: "Invalid or expired token" }, { status: 401 });
    }

    const columns = await Column.find({ userId: userIdFromAuth });
    return NextResponse.json({ columns }, { status: 200 });

  } catch (error) {
    console.error("Error retrieving columns:", error);
    return NextResponse.json({ message: "Error retrieving columns" }, { status: 500 });
  }
}
