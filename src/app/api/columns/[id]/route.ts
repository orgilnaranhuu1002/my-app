import { connectToMongoDB } from "@/dbConfig/dbconfig";
import Column from "@/models/column";
import Topic from "@/models/topic";
import { NextRequest } from "next/server";
import { ObjectId } from "mongodb";
import User from "@/models/userModel";
import bcrypt from "bcryptjs";

export async function DELETE(request: NextRequest, { params }: { params: { id: string } }) {
    const { id } = params;

    if (!id) {
        return new Response(JSON.stringify({ message: "ID is required" }), { status: 400, headers: { 'Content-Type': 'application/json' } });
    }

    try {
        await connectToMongoDB();

        // Find the column to get its type
        const column = await Column.findById(new ObjectId(id));
        if (!column) {
            return new Response(JSON.stringify({ message: "Column not found" }), { status: 404, headers: { 'Content-Type': 'application/json' } });
        }

        // Delete the column
        const deletedColumn = await Column.findByIdAndDelete(new ObjectId(id));
        if (!deletedColumn) {
            return new Response(JSON.stringify({ message: "Column not found" }), { status: 404, headers: { 'Content-Type': 'application/json' } });
        }

        // Delete associated topics
        const result = await Topic.deleteMany({ type: column.name });

        return new Response(JSON.stringify({ message: "Column and associated topics deleted", deletedTopicsCount: result.deletedCount }), { status: 200, headers: { 'Content-Type': 'application/json' } });
    } catch (error:any) {
        console.error("Error deleting column and topics:", error);
        return new Response(JSON.stringify({ message: "Error deleting column and topics", error: error.message }), { status: 500, headers: { 'Content-Type': 'application/json' } });
    }
}





export async function POST(request: NextRequest) {
    const { userId, password } = await request.json();

    if (!userId || !password) {
        return new Response(JSON.stringify({ message: "User ID and password are required" }), { status: 400, headers: { 'Content-Type': 'application/json' } });
    }

    try {
        await connectToMongoDB();
        const user = await User.findById(userId);

        if (!user) {
            return new Response(JSON.stringify({ message: "User not found" }), { status: 404, headers: { 'Content-Type': 'application/json' } });
        }

        const isPasswordValid = await bcrypt.compare(password, user.password);

        if (!isPasswordValid) {
            return new Response(JSON.stringify({ message: "Invalid password" }), { status: 401, headers: { 'Content-Type': 'application/json' } });
        }

        return new Response(JSON.stringify({ message: "Password verified" }), { status: 200, headers: { 'Content-Type': 'application/json' } });
    } catch (error) {
        console.error("Error verifying password:", error);
        return new Response(JSON.stringify({ message: "Error verifying password" }), { status: 500, headers: { 'Content-Type': 'application/json' } });
    }
}