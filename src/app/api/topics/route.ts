import {connectToMongoDB} from "@/dbConfig/dbconfig";
import Topic from "@/models/topic";
import { NextResponse , NextRequest } from "next/server";
import jwt, { JwtPayload } from 'jsonwebtoken';


export async function POST(request: NextRequest) {
  try {
    await connectToMongoDB();

    const token = request.cookies.get('token')?.value;
    if (!token) {
      return NextResponse.json({ message: "Authentication token is missing" }, { status: 401 });
    }

    let userIdFromAuth;
    try {
      const decoded = jwt.verify(token, process.env.TOKEN_SECRET!) as JwtPayload;
      userIdFromAuth = decoded._id;
    } catch (error) {
      return NextResponse.json({ message: "Invalid or expired token" }, { status: 401 });
    }

    const { title, description, type } = await request.json();
    const newTopic = await Topic.create({ title, description, type, userId: userIdFromAuth });

    return NextResponse.json({ message: "Topic Created", topic: newTopic }, { status: 201 });
  } catch (error) {
    console.error("Error creating topic:", error);
    return NextResponse.json({ message: "Error creating topic" }, { status: 500 });
  }
}

export async function GET(request: NextRequest) {
  try {
    await connectToMongoDB();

    const token = request.cookies.get('token')?.value;

    if (!token) {
      return NextResponse.json({ message: "Authentication token is missing" }, { status: 401 });
    }

    let userIdFromAuth;
    try {
     
      const decoded = jwt.verify(token, process.env.TOKEN_SECRET!) as JwtPayload;
      userIdFromAuth = decoded._id;
    } catch (error) {
      
      return NextResponse.json({ message: "Invalid or expired token" }, { status: 401 });
    }

 
    const topics = await Topic.find({ userId: userIdFromAuth });
    return NextResponse.json({ topics }, { status: 200 });

  } catch (error) {
    console.error("Error retrieving topics:", error);
    return NextResponse.json({ message: "Error retrieving topics" }, { status: 500 });
  }
}