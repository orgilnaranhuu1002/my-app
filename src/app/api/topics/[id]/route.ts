import {connectToMongoDB} from "@/dbConfig/dbconfig";
import Topic from "@/models/topic";
import { NextResponse , NextRequest } from "next/server";
import { ObjectId } from "mongodb";


// PUT: Update a topic by id
export async function PUT(request:NextRequest, { params }:{ params: { id: string } }) {
  const { id } = params;
  console.log("Received id:", id); 

  try {
    const { type, title, description , userId } = await request.json();
    await connectToMongoDB();
    const topicExists = await Topic.findById(id);
    if (!topicExists) {
      console.error("No topic found with the given ID:", id);
      return new Response("Topic not found", { status: 404 });
    }

    const updatedTopic = await Topic.findByIdAndUpdate(
      { _id: id, userId },
      { title, description, type },
      { new: true, runValidators: true }
    );

    if (!updatedTopic) {
      console.error("Failed to update topic with ID:", id);
      return new Response("Failed to update topic", { status: 400 });
    }

    console.log("Updated topic:", updatedTopic);
    return new Response(JSON.stringify(updatedTopic), {
      status: 200,
      headers: {
        'Content-Type': 'application/json'
      }
    });
  } catch (error) {
    console.error("Error updating topic:", error);
    return new Response(JSON.stringify({ error }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
}

// DELETE: Delete a topic by id

export async function DELETE(request:NextRequest, { params }:{ params: { id: string } }) {
    const { id } = params; 

    if (!id) {
        return new Response(JSON.stringify({ message: "ID is required" }), { status: 400, headers: { 'Content-Type': 'application/json' } });
    }

    try {
        await connectToMongoDB();
        const deletedTopic = await Topic.findByIdAndDelete(new ObjectId(id));
        if (!deletedTopic) {
            return new Response(JSON.stringify({ message: "Topic not found" }), { status: 404, headers: { 'Content-Type': 'application/json' } });
        }
        return new Response(JSON.stringify({ message: "Topic deleted" }), { status: 200, headers: { 'Content-Type': 'application/json' } });
    } catch (error) {
        console.error("Error deleting topic:", error);
        return new Response(JSON.stringify({ message: "Error deleting topic" }), { status: 500, headers: { 'Content-Type': 'application/json' } });
    }
}


