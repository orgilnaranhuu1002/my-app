'use client'

import React, { useState, useEffect } from 'react';
import Menu from "../../components/Menu/Menu";
import ActiveSprint from "../../components/ActiveSprint/ActiveSprint";
import More from "../../components/More/More";
import { ContextProvider } from "../../contexts/TopicContext";
import Footer from "../../components/Footer/Footer";
import LittleMenu from '../../components/Menu/LittleMenu';
import FloatingButton from '../../components/More/FloatingButton';
import MobileMid from '../../components/ActiveSprint/MobileMid';
import '../../globals.css'
import { useRouter } from 'next/navigation';
import TestDashboard from '@/app/components/Dashboard/TestDashboard';
import Isseus from '@/app/components/Issues/Isseus';
import Releases from '@/app/components/Releases/Releases';
import Roadmap from '@/app/components/Roadmap/Roadmap';
import Report from '@/app/components/Report/Report';
import Setting from '@/app/components/Setting/Setting';
import TeamMember from '@/app/components/TeamMember/TeamMember';
import Loader from '@/app/components/Loader';


const MainPage = () => {
  const [isWideScreen, setIsWideScreen] = useState(true);
  const [isMobile, setIsMobile] = useState(true);
  const router = useRouter();
  const [loading, setLoading] = useState<boolean>(false);

  const [activeComponent, setActiveComponent] = useState<string | false | Element>('ActiveSprint');
  useEffect(() => {
    const handleResize = () => {
      setIsWideScreen(window.innerWidth >= 1200);
      setIsMobile(window.innerWidth >= 900)
    };

    handleResize(); // Check initial screen width
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  const handleMenuClick = (componentName: string) => {
    setActiveComponent(componentName);
  };
  useEffect(() => {
    const userString = localStorage.getItem('User');

    if (userString) {
      const storedUser = JSON.parse(userString);

      const userId = storedUser._id;

      router.push(`/main/${userId}`);
    } else {
      router.push('/login');
      alert("Please login first");
    }
  }, [router]);

  return (

    <main className="bg-blue-800   flex justify-center items-center flex-col w-full h-full">
      {loading && <Loader />}
      <ContextProvider>

      <div className="flex rounded-tl-3xl flex-col min-h-screen  justify-between items-center w-full h-full bg-white">
        <div className="flex flex-row  justify-center items-start w-full h-full transition-all duration-300 ">
          {isWideScreen && <Menu onMenuClick={handleMenuClick} />}
          {!isWideScreen && isMobile && <LittleMenu onMenuClick={handleMenuClick} />}


            <div className='w-fit  bg-white   flex flex-col justify-center items-center'>
              {loading && <Loader />}

              {activeComponent === 'Dashboard' && <TestDashboard />}

              {activeComponent === 'ActiveSprint' && (isMobile ? <ActiveSprint /> : <MobileMid />)}
              {activeComponent === 'Issues' && <Isseus />}
              {activeComponent === 'Roadmap' && <Roadmap />}
              {activeComponent === 'Report' && <Report />}
              {activeComponent === 'Setting' && <Setting />}
              {activeComponent === 'TeamMember' && <TeamMember />}
              {activeComponent === 'Releases' && <Releases />}
            </div>

          {isWideScreen && <More />}
          {!isWideScreen && <FloatingButton />}


        </div>
        <div className="bg-white w-full h-auto flex flex-col items-center">
          {isMobile && <Footer />}
        </div>
      </div>
      </ContextProvider>

    </main>
  );
};

export default MainPage;



