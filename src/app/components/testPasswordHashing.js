import bcryptjs from 'bcryptjs';

const plainPassword = '99734404';

// Hash the password
bcryptjs.hash(plainPassword, 10, (err, hash) => {
  if (err) {
    console.error('Error hashing password:', err);
  } else {
    console.log('Hashed password:', hash);

    // Compare the plain password with the hashed password
    bcryptjs.compare(plainPassword, hash, (err, result) => {
      if (err) {
        console.error('Error comparing passwords:', err);
      } else {
        console.log('Password match:', result); // Should be true
      }
    });
  }
});
