import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import axios from 'axios';
import { FaAngleLeft } from 'react-icons/fa6';
import Image from 'next/image';
import hand from '@/app/public/src/hand.png';
import Loader from "../../../components/Loader"
import { EyeInvisibleOutlined, EyeTwoTone, LockOutlined, MailOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input } from 'antd';

const SignUp = () => {
    const router = useRouter();
    const [submitted, setSubmitted] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [loading, setLoading] = useState<boolean>(true);


    useEffect(() => {
        const userString = window.localStorage.getItem('User');
        if (userString) {
            const storedUser = JSON.parse(userString);
            const userId = storedUser._id;
            router.push(`/main/${userId}`);
        }
    }, [router]);

    const validateMessages = {
        required: '${label} is required!',
        types: {
            email: '${label} is not a valid email!',
        },
    };
    useEffect(() => {
        setLoading(false);
    }, []);

    const onFinish = async (values: any) => {
        setSubmitted(true);
        setLoading(true);

        try {
            const response = await axios.post('/api/users/signup', values);
            console.log('Signup successful', response.data);
            router.push('/login');
        } catch (error: any) {
            console.error('Failed to sign up the user', error.response?.data || error.message);
            if (error.response?.status === 500) {
                setErrorMessage("Internal Server Error. Please try again.");
            } else {
                setErrorMessage(error.response?.data?.error || "An error occurred.");
            }
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className='flex flex-row justify-center w-full min-h-screen h-full items-center'>
            {loading && <Loader/>}
            {errorMessage && (
                <div
                    onClick={() => setErrorMessage('')}
                    className='bg-gray-400 fixed align-middle bg-opacity-45 items-center justify-center flex z-50 w-full h-screen'>
                    <div className=" bg-red-100 border border-red-400  text-red-700 px-4 py-3 rounded  mb-4 max-w-[350px]">
                        <strong className="font-bold mr-2">Error!</strong>
                        <span className="block sm:inline">{errorMessage}</span>
                    </div>
                </div>
            )}
            <div className='w-1/3 flex justify-center flex-col gap-10 items-center'>
                <h1 className='text-black font-semibold text-[30px]'>#Design Ops</h1>
                <div className='flex justify-center w-[140px] h-[140px] items-center bg-blue-400 rounded-full border-blue-700 border-solid border-[4px]'>
                    <h1 className='flex justify-center w-full h-full text-[70px] text-white font-semibold items-center text-blue-white p-2 rounded-full'>
                        DO
                    </h1>
                </div>
                <Image
                    src={hand}
                    alt="hand"
                    width={100}
                    height={100}
                    className='absolute ml-36 mt-20'
                />
                <p className='text-blue-500'>Make free account and join us!</p>
            </div>
            <div className="flex flex-col items-center w-2/3 bg-blue-100 justify-center min-h-screen py-2">
                {loading && <Loader />}
                <h1 className="px-10 py-5 mb-10 bg-blue-500 rounded-full text-5xl">
                    Free Sign Up!
                    <span className="italic text-sm absolute bg-blue-50 p-4 rounded-full top-50 ml-4 text-blue-500">
                        Happy to have you!
                    </span>
                </h1>
                <Form
                    className="flex flex-col"
                    onFinish={onFinish}
                    name="nest-messages"
                    style={{ maxWidth: 600 }}
                    validateMessages={validateMessages}
                >
                    <Form.Item
                        name="username"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input
                            className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                            id="username"
                            type="text"
                            placeholder="Your Username..."
                            prefix={<UserOutlined />}
                        />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        rules={[
                            { type: 'email', message: 'The input is not valid E-mail!' },
                            { required: true, message: 'Please input your E-mail!' },
                        ]}
                    >
                        <Input
                            className="w-[350px] text-slate-800 bg-white p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                            id="email"
                            type="email"
                            placeholder="Your Email..."
                            prefix={<MailOutlined />}
                        />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            { required: true, message: 'Please input your password!' },
                            {min: 8, message: 'Password too short'},
                            {max: 20, message: 'Password too long'},

                        ]}
                        hasFeedback
                    >
                        <Input.Password
                            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                            className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                            id="password"
                            type="password"
                            placeholder="Input password"
                            prefix={<LockOutlined />}
                        />
                    </Form.Item>
                    <Form.Item
                        name="confirm"
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                            { required: true, message: 'Please confirm your password!' },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error('The passwords do not match!'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password
                            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                            placeholder='Confirm Password'
                            prefix={<LockOutlined />}
                            className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                        />
                    </Form.Item>
                    <Form.Item className='flex justify-center items-center'>
                        <Button
                            htmlType="submit"
                            className="bg-blue-600 border-0 m-0 w-fit h-fit text-white shadow-md shadow-gray-400 transition-all duration-300 min-w-[250px] max-w-[300px] p-2 rounded-full font-bold text-2xl cursor-pointer hover:opacity-80"
                        >
                            Sign Up
                        </Button>
                    </Form.Item>
                </Form>
                <Link href="/login" className='text-center flex-wrap mt-10 text-gray-500 p-4 max-w-[350px] flex flex-row justify-center items-center'>
                    <p>
                        Do you have a free account already?{' '}
                        <span className="font-bold text-blue-700 text-nowrap hover:text-blue-300 ml-2 cursor-pointer underline">
                            Login to your account
                        </span>
                    </p>
                </Link>
                <Link href="/">
                    <p className="mt-8 opacity-50 text-gray-500">
                        <FaAngleLeft className="inline mr-1" /> Back to the Homepage
                    </p>
                </Link>
            </div>
        </div>
    );
}

export default SignUp;
