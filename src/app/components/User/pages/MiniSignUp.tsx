'use client';

import Link from 'next/link';
import React, { useEffect } from 'react';
import { useRouter } from 'next/navigation';
import axios from 'axios';
import { FaAngleLeft } from 'react-icons/fa6';


const SignUp = () => {
    useEffect(() => {
        const userString = window.localStorage.getItem('User');
        if (userString) {
            const storedUser = JSON.parse(userString);

            const userId = storedUser._id;

            router.push(`/main/${userId}`);

        }
    }, []);
    const router = useRouter();

    const [user, setUser] = React.useState({
        username: '',
        email: '',
        password: '',
    });
    const [validation, setValidation] = React.useState({
        username: '',
        email: '',
        password: '',
    });
    const [submitted, setSubmitted] = React.useState(false);
    const [buttonDisabled, setButtonDisabled] = React.useState(false);
    const [loading, setLoading] = React.useState(false);

    const validateForm = () => {
        let usernameMsg = '';
        let emailMsg = '';
        let passwordMsg = '';

        if (user.username.length === 0) {
            usernameMsg = 'Username cannot be empty.';
        }
        if (!/^[^@]+@\w+(\.\w+)+\w$/.test(user.email)) {
            emailMsg = 'Please enter a valid email address.';
        }
        if (user.password.length < 8) {
            passwordMsg = 'Password is too short.';
        } else if (user.password.length > 20) {
            passwordMsg = 'Password is too long.';
        }

        setValidation({ username: usernameMsg, email: emailMsg, password: passwordMsg });
        return usernameMsg.length === 0 && emailMsg.length === 0 && passwordMsg.length === 0;
    };
    const onSignUp = async () => {
        setSubmitted(true);

        const isFormValid = validateForm();
        if (!isFormValid) {
            setButtonDisabled(true);
            return;
        }

        setButtonDisabled(false);
        try {
            setLoading(true);
            const response = await axios.post('/api/users/signup', user);
            console.log('signup okay', response.data);
            router.push('/login');
        } catch (error: any) {
            console.error('Failed to sign up the user', error.response?.data || error.message);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {

        if (submitted) {
            validateForm();
        }
    }, [user]);


    return (
        <div className='flex flex-row justify-center bg-blue-100 w-full min-h-screen min-w-[400px] items-center'>



            <div className="flex flex-col items-center w-full h-full  bg-blue-100  justify-center min-h-screen py-2">
                <div className='w-1/3 flex justify-center flex-col gap-10 items-center'>
                    <h1 className='text-black font-semibold min-w-[200px] text-[30px]'>#Design Ops</h1>

                    <div className='flex justify-center w-[140px] h-[140px]  items-center  bg-blue-400 rounded-full border-blue-700  border-solid border-[4px]'>
                        <h1 className='flex justify-center w-full h-full text-[70px] text-white font-semibold items-center text-blue-white p-2 rounded-full'>
                            DO
                        </h1>

                    </div>

                    <p className='text-blue-500 min-w-[300px] flex justify-center items-center'>Make free account and join us!</p>
                </div>
                <h1 className="px-10 py-5  bg-blue-300 min-w-[300px]  flex flex-row justify-end items-end  rounded-full text-5xl">

                    {loading ? 'Processing...' : 'Free Sign Up'}

                </h1>
                <span className="italic text-sm flex item-center justify-center bg-blue-50 p-2 w-1/6 min-w-[200px] b-10 rounded-full ml-40 text-blue-500">
                    Happy to have you!
                </span>

                <input
                    className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                    id="username"
                    type="text"
                    value={user.username}
                    onChange={(e) => setUser({ ...user, username: e.target.value })}
                    placeholder="Your Username..."
                />
                {submitted && validation.username && <p className="text-red-500">{validation.username}</p>}


                <input
                    className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                    id="email"
                    type="text"
                    value={user.email}
                    onChange={(e) => setUser({ ...user, email: e.target.value })}
                    placeholder="Your Email..."
                />
                {submitted && validation.email && <p className="text-red-500">{validation.email}</p>}


                <input
                    className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                    id="password"
                    type="password"
                    value={user.password}
                    onChange={(e) => setUser({ ...user, password: e.target.value })}
                    placeholder="Your Password..."
                />
                {submitted && validation.password && <p className="text-red-500">{validation.password}</p>}


                <button
                    onClick={onSignUp}
                    className="bg-blue-500 shadow-md shadow-gray-400 transition-all duration-300 w-auto min-w-[250px] max-w-[300px] px-10 py-5 rounded-full font-bold text-2xl cursor-pointer hover:opacity-80">
                    Sign Up
                </button>
                <Link href="/login" className='  text-center flex-wrap mt-10 text-gray-500 p-4 max-w-[350px] flex flex-row justify-center items-center'>
                    <p >
                        Do you have a free account already?{' '}
                        <span className="font-bold text-blue-700 text-nowrap hover:text-blue-300 ml-2 cursor-pointer underline">
                            Login to your account
                        </span>
                    </p>
                </Link>

                <Link href="/">
                    <p className="mt-8 opacity-50 text-gray-500">
                        <FaAngleLeft className="inline mr-1" /> Back to the Homepage
                    </p>
                </Link>
            </div>
        </div>
    );
}
export default SignUp