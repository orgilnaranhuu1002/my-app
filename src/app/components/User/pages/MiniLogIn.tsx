'use client';

import Link from 'next/link';
import React, { useEffect } from 'react';
import { useRouter } from 'next/navigation';
import axios from 'axios';
import { FaAngleLeft } from 'react-icons/fa6';
import Cookies from 'js-cookie';
import Loader from '../../Loader';
const LogIn = () => {
    const router = useRouter();
    
    useEffect(() => {
        const userString = window.localStorage.getItem('User');
        if (userString) {
            const storedUser = JSON.parse(userString);

            const userId = storedUser._id;

            router.push(`/main/${userId}`);

        }
    }, []);
    const [errorMessage, setErrorMessage] = React.useState('');
    const [user, setUser] = React.useState({

        email: '',
        password: '',
    });
    const [buttonDisabled, setButtonDisabled] = React.useState(false);
    const [loading, setLoading] = React.useState(false);

    const onLogin = async () => {
        try {
            setLoading(true);
            const response = await axios.post('api/users/login', user);

            window.localStorage.setItem('User', JSON.stringify(response.data.user));

            console.log('Login successful', response.data);
            router.push(`/main/${response.data.user.id}`);
        } catch (error: any) {
            setLoading(false);
            if (error.response && error.response.status === 404) { 
                setErrorMessage('Login failed. Please try again.');
            } else {
                setErrorMessage("User doesn't exist.");
                console.log('Login failed', error.message);
            }
        }
    };


    useEffect(() => {
        if (user.email.length > 0 && user.password.length > 0) {
            setButtonDisabled(false);
        } else {
            setButtonDisabled(true);
        }
    }, [user]);

    console.log(user);

    return (
        <div className='flex flex-row justify-center bg-blue-100 w-full h-full min-h-screen min-w-[400px] items-center'>


{loading && <Loader />}
            <div className="flex flex-col items-center w-full h-full  bg-blue-100  justify-center min-h-screen ">

                <div className='w-full flex justify-center flex-col gap-10 items-center'>
                    <h1 className='text-black font-semibold text-[30px]'>#Design Ops</h1>

                    <div className='flex justify-center w-[140px] h-[140px]  items-center  bg-blue-400 rounded-full border-blue-700  border-solid border-[4px]'>
                        <h1 className='flex justify-center w-full h-full text-[70px] text-white font-semibold items-center text-blue-white p-2 rounded-full'>
                            DO
                        </h1>

                    </div>

                    <p className='text-blue-500 text-wrap w-full min-w-[400px]  text-center '> Log In to your account.</p>
                </div>
                <h1 className="w-fit py-5 px-2 justify-center  bg-blue-300 min-w-[300px]  text-nowrap flex flex-row  items-center text-center rounded-full text-4xl">

                Welcome back!

                </h1>
                <span className="italic text-sm bg-blue-50 p-4 rounded-full ml-40  text-blue-500">
                    Happy to see you again!
                </span>

                <input
                    className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                    id="email"
                    type="text"
                    value={user.email}
                    onChange={(e) => setUser({ ...user, email: e.target.value })}
                    placeholder="Your Email..."
                />

                <input
                    className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                    id="password"
                    type="password"
                    value={user.password}
                    onChange={(e) => setUser({ ...user, password: e.target.value })}
                    placeholder="Your Password..."
                />

                <button
                    onClick={onLogin}
                    className="bg-blue-500 shadow-md text-white shadow-gray-400 transition-all duration-300 w-auto min-w-[250px] max-w-[300px] px-10 py-5 rounded-full font-bold text-2xl cursor-pointer hover:opacity-80">                  
                      Log In 
                </button>
                <Link href="/sign-up" className='  text-center flex-wrap mt-10 text-gray-500 p-4 max-w-[350px] flex flex-row justify-center items-center'>
                    <p >
                        Do not have an account yet?

                        <span className="font-bold text-blue-700 text-nowrap hover:text-blue-300 ml-2 cursor-pointer underline">
                            Register your free account now

                        </span>
                    </p>
                </Link>

                <Link href="/">
                    <p className="mt-8 opacity-50 text-gray-600">
                        <FaAngleLeft className="inline mr-1" /> Back to the Homepage
                    </p>
                </Link>
            </div>
        </div>
    );

}

export default LogIn