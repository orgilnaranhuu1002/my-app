'use client';

import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import axios from 'axios';
import { FaAngleLeft } from 'react-icons/fa6';
import Image from 'next/image';
import hand from '@/app/public/src/hand.png';
import Loader from '../../../components/Loader';
import { MailOutlined, EyeTwoTone, EyeInvisibleOutlined, LockOutlined } from '@ant-design/icons';

import type { FormProps } from 'antd';
import { Button, Form, Input } from 'antd';





type FieldType = {
    username?: string;
    password?: string;
    remember?: string;
};

const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
    console.log('Success:', values);
};

const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
    console.log('Failed:', errorInfo);
};

const LogIn = () => {

    const router = useRouter();
    const [loading, setLoading] = useState<boolean>(true);
    const [errorMessage, setErrorMessage] = React.useState('');
    const [user, setUser] = React.useState({

        email: '',
        password: '',
    });


    useEffect(() => {
        const userString = window.localStorage.getItem('User');

        if (userString) {
            const storedUser = JSON.parse(userString);

            const userId = storedUser._id;

            router.push(`/main/${userId}`);
        }
    }, []);
    useEffect(() => {
        setLoading(false);
    }, []);
    


    const onLogin = async () => {
        try {
            setLoading(true);
            const response = await axios.post('/api/users/login', user);

            if (response.data.success) {
                const userData = {
                    ...response.data.user,
                    profileImage: response.data.profileImage 
                };
                window.localStorage.setItem('User', JSON.stringify(userData));
            
                console.log('Login successful', response.data);
                console.log('User data', userData);
                router.push(`/main/${userData._id}`);

            } else {
                setLoading(false);
                setErrorMessage(response.data.error || 'An error occurred. Please try again.');
            }
        } catch (error: any) {
            setLoading(false);
            console.error('Error during login:', error);
            if (error.response && error.response.status === 400) {
                setErrorMessage("User doesn't exist or invalid credentials.");
            } else {
                setErrorMessage('An error occurred. Please try again.');
            }
        }
    };

    return (
        <div className='flex flex-row justify-center w-full min-h-screen h-full items-center'>
            {loading && <Loader />}
            
            {errorMessage && (
                <div
                    onClick={() => setErrorMessage('')}
                    className='bg-gray-400 fixed align-middle bg-opacity-45 items-center justify-center flex z-50 w-full h-screen '>
                    <div className=" bg-red-100 border border-red-400  text-red-700 px-4 py-3 rounded  mb-4 max-w-[350px]">
                        <strong className="font-bold mr-2">Error!</strong>
                        <span className="block sm:inline">{errorMessage}</span>
                    </div>
                </div>
            )}
            <div className='w-1/3 flex justify-center flex-col gap-10 items-center'>
                <h1 className='text-black font-semibold text-[30px]'>#Design Ops</h1>

                <div className='flex justify-center w-[140px] h-[140px]  items-center  bg-blue-400 rounded-full border-blue-700  border-solid border-[4px]'>
                    <h1 className='flex justify-center w-full h-full text-[70px] text-white font-semibold items-center text-blue-white p-2 rounded-full'>
                        DO
                    </h1>

                </div>
                <Image
                    src={hand}
                    alt="hand"
                    width={100}
                    height={100}
                    className='absolute ml-36 mt-16'
                />
                <p className='text-blue-500 text-wrap max-w-[300px] text-center '> Log In to your account.</p>
            </div>
            <div className="flex w-2/3 bg-blue-100 flex-col items-center justify-center  text-black  min-h-screen py-2">
                {loading ? <Loader /> : null}

                <h1 className="px-10 py-5  mb-20 bg-blue-500  rounded-full text-5xl text-white ">
                    Welcome back!
                    <span className="italic text-sm absolute bg-blue-50 p-4 rounded-full top-50 ml-4 text-blue-500">
                        Happy to see you again!
                    </span>
                </h1>

                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    style={{ maxWidth: 600 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    className='flex flex-col items-center w-[400px] justify-center gap-2 min-w-[400px] max-w-[400px] '
                >
                    <Form.Item name={['user', 'name']} rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                    ]}>
                        <Input
                            className="w-[350px] text-slate-800 bg-white p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                            id="email"
                            type="text"
                            value={user.email}
                            onChange={(e) => setUser({ ...user, email: e.target.value })}
                            placeholder="Your Email..."
                            prefix={<MailOutlined />}
                        />
                    </Form.Item>



                    <Form.Item
                        name="password"

                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                        hasFeedback
                    >

                        <Input.Password
                            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                            className="w-[350px] text-slate-800 p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600"
                            id="password"
                            type="password"
                            value={user.password}
                            onChange={(e) => setUser({ ...user, password: e.target.value })}
                            placeholder="input password"
                            prefix={<LockOutlined />}

                        />
                    </Form.Item>
                    <Form.Item className='flex justify-center p-0 m-0 items-center w-full'>
                        <Button
                            onClick={onLogin}

                            className="bg-blue-600 shadow-md border-0 text-white shadow-gray-400 transition-all duration-300  min-w-[250px] max-w-[300px] w-fit h-fit rounded-full font-bold p-2 text-2xl cursor-pointer hover:opacity-80">
                            Log In
                        </Button>
                    </Form.Item>
                </Form>
                <Link href="/sign-up" className='  text-center flex-wrap mt-10 text-gray-500 p-4 max-w-[350px] flex flex-row justify-center items-center'>
                    <p >
                        Do not have an account yet?

                        <span className="font-bold text-blue-700 text-nowrap hover:text-blue-300 ml-2 cursor-pointer underline">
                            Register your free account now

                        </span>
                    </p>
                </Link>


                <Link href="/">
                    <p className="mt-8 opacity-50 text-gray-600">
                        <FaAngleLeft className="inline mr-1" /> Back to the Homepage
                    </p>
                </Link>
            </div>
        </div>
    );

}

export default LogIn