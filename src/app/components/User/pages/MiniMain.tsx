import Link from "next/link"
import { useRouter } from "next/navigation";
import { useEffect } from "react";

const MiniMain = () => {
	const router = useRouter();
    useEffect(() => {
        const userString = window.localStorage.getItem('User');
    
        if (userString) {
            const storedUser = JSON.parse(userString);
    
            const userId = storedUser._id;
    
            router.push(`/main/${userId}`);
        }
    }, []);
  return (
	
    <div className=" flex flex-col justify-center min-w-[320px] bg-white w-full h-full min-h-screen items-center">
			
				<div className='flex flex-col justify-center w-full gap-20 items-center'>
				<div className='text-gray-600 flex flex-row w-full justify-center gap-6 items-center'>
				<div className='flex justify-center w-[100px] h-[100px]  items-center  bg-blue-400 rounded-full border-blue-700  border-solid border-[4px]'>
                    <h1 className='flex justify-center w-full h-full text-[40px] text-white font-semibold items-center text-blue-white p-2 rounded-full'>
                        DO
                    </h1>
                </div>
				<div className='flex flex-col items-end'>
				<h1 className='text-[30px] font-semibold'>
					#Design Ops
				</h1>
				<p className='text-auto'>2024 </p>
				</div>
				</div>
				<Link href="/sign-up">
				<button className="bg-blue-500 py-5 rounded-full w-auto min-w-[250px] max-w-[300px] px-10 font-bold text-2xl cursor-pointer hover:opacity-80">
					Join Us!
				</button>
				</Link>
				</div>
			
	
		</div>
  )
}

export default MiniMain