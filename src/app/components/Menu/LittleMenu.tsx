import React, { useState } from 'react';
import { AiOutlineFundProjectionScreen } from 'react-icons/ai'
import { GoPeople } from 'react-icons/go'
import { IoMapOutline, IoSettingsOutline } from 'react-icons/io5'
import { PiStackFill, PiSquaresFour, PiTicket, PiAirplaneTakeoffLight, PiSquaresFourThin } from 'react-icons/pi'
import "../../globals.css"
interface MenuProps {
  onMenuClick: (componentName: string) => void;
  // onMenuClick: (componentName: string,url: string) => void;

}
const LittleMenu: React.FC<MenuProps> = ({ onMenuClick }) => {
  const [activeMenu, setActiveMenu] = useState<string>('ActiveSprint'); 

  const handleMenuClick = (componentName: string) => {
    setActiveMenu(componentName); 
    onMenuClick(componentName);   
  };
  return (
    <div className='bg-white flex rounded-tl-[100px] min-w-[80px] mr-2 flex-col h-auto justify-center mt-7 items-center max-w-[100px] '>
      <div className='flex justify-center rounded-full w-[70px] h-[70px] p-[10px] mt-2 pb-3 bg-indigo-50 items-center mb-3 '>
        <div className='flex justify-center items-center w-full h-full bg-blue-400 rounded-full border-blue-700  border-solid border-[4px]'>
          <h1 className='flex justify-center w-full h-full text-[15px]  font-semibold items-center text-blue-white p-2 rounded-full'>
            DO
          </h1>
        </div>
      </div>
      {/* Menu section */}
      <div className="text-gray-500 flex flex-col gap-3">


        <button onClick={() => handleMenuClick('Dashboard')}
          className={`lilMenuItems ${activeMenu === 'Dashboard' ? 'activeMenu' : ''} flex justify-center items-center`}><PiSquaresFourThin className='w-[26px] h-[26px] ' values='Dashboard' /></button>
        {/* <button onClick={() => onMenuClick('Dashboard','/dashboard')} className='menuItems'><PiSquaresFourThin className='w-[26px] h-[26px] ml-[5px]' />Dashboard</button> */}


        <button onClick={() => handleMenuClick('Roadmap')}
          className={`lilMenuItems ${activeMenu === 'Roadmap' ? 'activeMenu' : ''} flex justify-center items-center`}><IoMapOutline className='w-[22px] h-[22px]' /></button>

        <button onClick={() => handleMenuClick('ActiveSprint')}
          className={`lilActiveMenu ${activeMenu === 'ActiveSprint' ? 'activeMenu' : ''}flex justify-center items-center`}><PiStackFill className=' w-[22px] h-[22px]' /></button>

        <button onClick={() => handleMenuClick('Report')}
          className={`lilMenuItems ${activeMenu === 'Report' ? 'activeMenu' : ''}flex justify-center items-center`}><AiOutlineFundProjectionScreen className=' w-[22px] h-[22px]' /></button>
        <button onClick={() => handleMenuClick('Issues')}
          className={`lilMenuItems ${activeMenu === 'Issues' ? 'activeMenu' : ''}flex justify-center items-center`}><PiTicket className='w-[22px] h-[22px]  ' /></button>
        <button onClick={() => handleMenuClick('Releases')}
          className={`lilMenuItems ${activeMenu === 'Releases' ? 'activeMenu' : ''}flex justify-center items-center`}><PiAirplaneTakeoffLight className=' w-[22px] h-[22px] ' /></button>
        <button onClick={() => handleMenuClick('TeamMember')}
          className={`lilMenuItems ${activeMenu === 'TeamMember' ? 'activeMenu' : ''}flex justify-center items-center`}><GoPeople className=' w-[22px] h-[22px] ' /></button>
        <button onClick={() => handleMenuClick('Setting')}
          className={`lilMenuItems ${activeMenu === 'Setting' ? 'activeMenu' : ''}flex justify-center items-center`}><IoSettingsOutline className='w-[22px] h-[22px] ' /></button>
      </div>
    </div>

  )
};

export default LittleMenu;
