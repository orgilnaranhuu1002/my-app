import React, { useState } from 'react'
import { AiOutlineFundProjectionScreen } from 'react-icons/ai'
import { GoPeople } from 'react-icons/go'
import { IoMapOutline, IoSettingsOutline } from 'react-icons/io5'
import { PiStackFill, PiSquaresFourThin, PiTicket, PiAirplaneTakeoffLight } from 'react-icons/pi'
import "../../globals.css"


interface MenuProps {
  onMenuClick: (componentName: string) => void;
  // onMenuClick: (componentName: string,url: string) => void;

}
const Menu: React.FC<MenuProps>  = ({ onMenuClick }) => {
  const [activeMenu, setActiveMenu] = useState<string>('ActiveSprint'); // State to keep track of the active menu

  const handleMenuClick = (componentName: string) => {
    setActiveMenu(componentName); // Set the current menu as active
    onMenuClick(componentName);   // Call the prop function to handle the click
  };
  return (
    <div className='bg-white flex h-full  rounded-tl-[40px] flex-col  justify-start   w-[200px] min-w-[200px] pt-6 pl-2'>
      <h1 className='font-bold text-black text-xl pl-4  pb-12 items-start '>#Design Ops</h1>
      {/* Menu section */}
      <div className="text-gray-500 flex flex-col gap-3">
      
      <button onClick={() => handleMenuClick('Dashboard')}
          className={`menuItems ${activeMenu === 'Dashboard' ? 'activeMenu' : ''}`}><PiSquaresFourThin className='w-[26px] h-[26px] ml-[5px]' /><p>Dashboard</p></button>
      {/* <button onClick={() => onMenuClick('Dashboard','/dashboard')} className='menuItems'><PiSquaresFourThin className='w-[26px] h-[26px] ml-[5px]' />Dashboard</button> */}

  
        <button onClick={() => handleMenuClick('Roadmap')}
          className={`menuItems ${activeMenu === 'Roadmap' ? 'activeMenu' : ''}`}><IoMapOutline className='w-[22px] h-[22px] ml-[6px]' /><p>Roadmap</p></button>
      
        <button onClick={() => handleMenuClick('ActiveSprint')}
          className={`menuItems ${activeMenu === 'ActiveSprint' ? 'activeMenu':'' }`}><PiStackFill className=' w-[22px] h-[22px] ml-[6px]' /><p>Active Sprint</p></button>
  
        <button onClick={() => handleMenuClick('Report')}
          className={`menuItems ${activeMenu === 'Report' ? 'activeMenu' : ''}`}><AiOutlineFundProjectionScreen className=' w-[22px] h-[22px] ml-[6px]' /><p>Report</p></button>
        <button onClick={() => handleMenuClick('Issues')}
          className={`menuItems ${activeMenu === 'Issues' ? 'activeMenu' : ''}`}><PiTicket className='w-[22px] h-[22px] ml-[6px] ' /><p>Issues</p></button>
        <button onClick={() => handleMenuClick('Releases')}
          className={`menuItems ${activeMenu === 'Releases' ? 'activeMenu' : ''}`}><PiAirplaneTakeoffLight className=' w-[22px] h-[22px] ml-[6px]' /><p>Releases</p></button>
        <button onClick={() => handleMenuClick('TeamMember')}
          className={`menuItems ${activeMenu === 'TeamMember' ? 'activeMenu' : ''}`}><GoPeople className=' w-[22px] h-[22px] ml-[6px]' /><p>Team member</p></button>
        <button onClick={() => handleMenuClick('Setting')}
          className={`menuItems ${activeMenu === 'Setting' ? 'activeMenu' : ''}`}><IoSettingsOutline className='w-[22px] h-[22px] ml-[6px]' /><p>Setting</p></button>
      </div>
    </div>

  )
}

export default Menu