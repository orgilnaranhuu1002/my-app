"use client"

import React, { useContext, useEffect, useState } from 'react';
import { CiSearch } from 'react-icons/ci';
import { FaPlus } from 'react-icons/fa';
import Image from "next/image";
import friend4 from "../../public/src/man 1.jpg";
import friend2 from "../../public/src/girl 2.jpg";
import friend3 from "../../public/src/man 2.jpg";
import friend1 from "../../public/src/man 3.jpg";
import { CreateModal } from './CreateModal';
import { Modal, TextInput } from 'flowbite-react';
import { Context } from '../../contexts/TopicContext';
import { DragDropContext, Draggable, Droppable } from '@hello-pangea/dnd';
import "../../globals.css";
import Loader from '../Loader';
import { useRouter } from 'next/navigation';
import { BsThreeDots } from 'react-icons/bs';
import { Alert } from "flowbite-react";
import { ScrollShadow } from "@nextui-org/scroll-shadow";

const ActiveSprint = () => {
  const {
    onCloseModal, columns, setOpenModal, id, userId, openModal, handleAddTodo, title, topics, setTitle, editedBlog, 
    description, setDescription, setType, type, handleColumnClick, handleDragEnd, loading, handleRemoveTodo, getBackgroundColor, setEditedBlog, handleEditTodo, handleBlogClick, selectedBlogIndex, selectedColumnIndex, handleRemoveColumn,
  } = useContext(Context);


  const router = useRouter();
  const [openTask, setOpenTask] = useState(false);
  const [openConfirm, setOpenConfirm] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [alertColor, setAlertColor] = useState('success');
  const [password, setPassword] = useState("");

  const handlecheckPassword = (e: any) => {
    setPassword(e.target.value);
  };

  useEffect(() => {
    if (showAlert) {
      const timer = setTimeout(() => {
        setShowAlert(false);
      }, 3000);

      return () => clearTimeout(timer);
    }
  }, [showAlert]);
  const handleAddTopicSuccess = () => {
    setAlertMessage('Post added successfully!');
    setAlertColor('success');
    setShowAlert(true);
  };
  const handleDeleteTopicSuccess = () => {
    setAlertMessage('Post deleted successfully!');
    setAlertColor('success');
    setShowAlert(true);
  };
  const handleEditTopicSuccess = () => {
    setAlertMessage('Post edited successfully!');
    setAlertColor('success');
    setShowAlert(true);
  };

  const handleDeleteColumnSuccess = () => {
    setAlertMessage('Post deleted successfully!');
    setAlertColor('success');
    setShowAlert(true);
  };

  const handleDeleteColumn = async () => {
    const selectedColumn = columns[selectedColumnIndex];
    if (selectedColumn && selectedColumn.id) {
      try {
        handleRemoveColumn(selectedColumn.id, password);
        handleDeleteColumnSuccess();
        onCloseConfirm();
      } catch (error) {
        console.error("Error deleting column:", error);
      }

    } else {
      console.error("Error: Selected column object is invalid or missing id");
      alert("Incorrect password");

    }
  };
  const onCloseTask = () => {
    setOpenTask(false);
    setDescription('');
    setType('');
    setTitle('');
  };
  const onCloseConfirm = () => {
    setOpenConfirm(false);
    setPassword("");
  };

  useEffect(() => {
    const userString = window.localStorage.getItem('User');
    if (userString) {
      const storedUser = JSON.parse(userString);
      const userId = storedUser._id;
      router.push(`/main/${userId}`);
    }
  }, [router]);
  const People = () => {
    router.push(`/people`);

  }

  return (

    <div className='min-w-[872px] w-full bg-white max-w-[960px]  h-full flex flex-col justify-start items-center'>
      {loading && <Loader />}
      {showAlert && (
        <div className="fixed z-50 top-5 left-1/2 w-[400px]  transform -translate-x-1/2">
          <Alert
            color={alertColor}
            onDismiss={() => setShowAlert(false)}
            className='text-center shadow-xl shadow-gray-100 '
          >
            {alertMessage}
          </Alert>
        </div>
      )}
      {/* Search */}
      <div className='flex flex-row justify-between align-bottom w-full h-20 px-10'>
        <p className='text-gray-600 text-lg flex items-center justify-center'>Active Sprint</p>
        <div className='bg-inherit w-[220px] border-b border-slate-300 h-1/2 mt-6 align-bottom justify-center items-center flex'>
          <input name='search' className='bg-inherit text-xs text-gray-600 outline-none w-[160px]' placeholder="Search task, project, label..." />
          <button className='w-[28px] h-[28px] text-gray-400'>
            <CiSearch className='w-full h-full' />
          </button>
        </div>
      </div>
      {/* Main */}
      <div className='w-full h-screen bg-[#f0f5ff] max-h-[800px]  rounded-[40px] items-center align-middle flex flex-col'>
        {/* Create task and friends */}
        <div className='flex flex-row justify-between items-center px-[70px] mt-5 mb-3 h-[60px] w-full'>
          <div className='flex flex-row gap-2 items-center justify-start'>
            <Image className='w-[35px] h-[35px] rounded-full' src={friend1} alt='faceless' />
            <Image className='w-[35px] h-[35px] rounded-full' src={friend3} alt='faceless' />
            <Image className='w-[35px] h-[35px] rounded-full' src={friend2} alt='faceless' />
            <Image className='w-[35px] h-[35px] rounded-full' src={friend4} alt='faceless' />
            {/* <Link href={"/invite"}> */}
            <button onClick={People} className="w-[120px] shadow-lg transition duration-300 ease-in-out text-sm font-semibold h-[30px] ml-2 shadow-gray-300 rounded-2xl bg-white text-black border-0 hover:bg-blue-200 hover:text-blue-600">
              Invite People
            </button>
            {/* </Link> */}
          </div>
          {/* CreateTask */}
          <div className='flex justify-center items-center'>
            <CreateModal />
          </div>
        </div>

        <div className='flex w-full h-auto p-0 min-w-[300px] max-w-[850px] justify-center min-h-[500px] max-h-[650px] bg-[#f0f5ff] '>
          <ScrollShadow offset={100} size={80} orientation="horizontal" className='overflow-scroll h-auto gap-5  flex-row flex justify-start min-h-[600px] max-h-[650px] pl-4 w-full'>
            <DragDropContext onDragEnd={handleDragEnd}>
              {columns.map((column, index) => (
                <Droppable key={column.id} droppableId={column.name}>
                  {(provided) => (

                    <div {...provided.droppableProps} ref={provided.innerRef} className='flex flex-col align-middle w-1/4 items-center transition-all duration-300'>
                      {loading && <Loader />}

                      <div className='flex flex-col mb-5  align-middle  items-center w-[190px] button-container bg-[#f0f5ff]'>
                        <div className='flex flex-col justify-end align-middle h-[60px] w-full items-center'>
                          <button className="shadow-gray-300 h-[42px] shadow-md overflow-hidden overflow-ellipsis text-black border-0 text-base w-full font-semibold pl-4 rounded-[8px] items-center flex justify-between transition duration-300 ease-in-out bg-white hover:bg-blue-200 hover:text-blue-600"
                            onClick={() => {
                              handleColumnClick(index)
                              setOpenModal(true);
                              setType(column.name);
                            }}>
                            <div className='overflow-hidden overflow-y-hidden overflow-x-hidden justify-start flex-row flex items-start  overflow-ellipsis max-w-[70%]'>{column.name}</div>
                            <div className='bg-blue-200 text-blue-400 rounded-full w-[25px] h-[25px] mr-3 flex items-center justify-center'>
                              <FaPlus className="h-[13px] w-[13px]" />
                            </div>
                          </button>
                        </div>
                      </div>
                      <div className='flex flex-col align-middle w-full items-center transition-all duration-300'>
                        <ul className="items-center flex flex-col gap-5 w-full h-auto justify-center">
                          {(topics || []).map((topic, index) => {
                            if (topic.type === column.name) {
                              return <>
                                {loading && <Loader />}
                                <Draggable key={topic.id} draggableId={`topic-${index}`} index={index}>
                                  {(provided) => (
                                    <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} className='w-full flex flex-col justify-start min-h-[110px] items-center'>
                                      <div className='bg-white min-h-[90px] w-[190px] h-auto rounded-2xl flex shadow-lg shadow-gray-300 md:break-words flex-col justify-start items-center'>
                                        <div className='flex flex-row justify-between w-full items-center'>
                                          <span className={`bg-${getBackgroundColor(topic.title)} flex justify-center capitalize min-w-[40px] items-center text-gray-700 md:break-words flex-row text-[11px] h-[18px] px-2 rounded-[4px] m-2 `} key={topic.id} id={topic.title}>
                                            {topic.title}
                                          </span>
                                          <button onClick={() => {
                                            handleBlogClick(index)
                                            setOpenTask(true)
                                          }} className="text-gray-400 items-center flex justify-center w-[50px] h-[20px]">
                                            <BsThreeDots className='transition-color duration-500 hover:text-black' />
                                          </button>
                                        </div>
                                        <div className='w-full h-auto flex flex-col justify-start items-center rounded-none'>
                                          <span className='text-gray-700 h-auto max-h-[160px] max-w-[90%] text-wrap overflow-ellipsis overflow-hidden break-all w-[90%] mb-3 flex flex-row text-xs justify-start items-start' key={topic.id} id={topic.description}>
                                            {topic.description}
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  )}
                                </Draggable>
                              </>
                            }
                          }
                          )}
                        </ul>



                        <Modal show={openTask} size="md" onClose={() => { onCloseTask() }} popup>
                          <Modal.Header />
                          <Modal.Body>
                            <div className=" flex h-fit flex-col ">
                              <div className="text-xl font-medium text-gray-900 max-h-screen flex flex-col gap-2 w-full p-4  rounded-xl shadow-sm shadow-gray-400 dark:text-white">
                                <div className="flex flex-col gap-2 w-full">
                                  <div className='flex flex-row justify-between'>
                                    <h1>Type</h1>
                                  </div>
                                  <select value={editedBlog?.type || "Backlog"} onChange={(e) => setEditedBlog({ ...editedBlog, type: e.target.value })}
                                    className="w-full bg-blue-300 p-4 px-6 text-[20px]  break-all max-w-[320px]  shadow-lg shadow-gray-300  text-white outline-white rounded-2xl focus:outline-none focus:ring-0 border-none hover:border-none">
                                    {columns.map((column) => (
                                      <option className='break-all !max-w-[300px]' key={column.name} value={column.name}>{column.name}</option>

                                    ))}

                                  </select>
                                  <p className='text-[12px]  text-gray-500'>You can change your card type</p>
                                </div>
                                <div className='w-full  flex flex-col '>
                                  <h1>Title</h1>
                                  <div className='flex flex-col gap-1'>
                                    <select value={editedBlog?.title || ""} onChange={(e) => setEditedBlog({ ...editedBlog, title: e.target.value })}
                                      className="w-full bg-blue-300 p-4 px-6 text-[20px] capitalize   shadow-lg shadow-gray-300 max-w-xs text-white outline-white rounded-2xl focus:outline-none focus:ring-0 border-none hover:border-none">
                                      <option selected value="">{editedBlog?.title}</option>
                                      <option value="Banner">Banner</option>
                                      <option value="Illustration">Illustration</option>
                                      <option value="Video">Video</option>
                                      <option value="UI UX">UI UX</option>
                                    </select>
                                    <p className='text-[12px] mb-2 text-gray-500'>Select and change your title</p>
                                    <input type="text" value={editedBlog?.title || ""} onChange={(e) => setEditedBlog({ ...editedBlog, title: e.target.value })}
                                      className="w-fit p-4 text-sm text-gray-700 hover:ring-blue-200 ring-2 ring-gray-100 bg-white border rounded-xl border-white focus:ring-blue-300 focus:border-blue-300 block transition duration-150 ease-in-out" />
                                    <p className='text-[12px] mb-2 text-gray-500'>You can change your title into new one</p>
                                  </div>
                                </div>
                                <div className='flex justify-start items-start w-full'>
                                  <div className='w-full h-auto p-0 flex gap-2 flex-col justify-start items-start rounded-none'>
                                    <div className='flex flex-row w-full  justify-between'>
                                      <h1>Description</h1>
                                    </div>
                                    <textarea
                                      value={editedBlog?.description || ""}
                                      onChange={(e) => setEditedBlog({ ...editedBlog, description: e.target.value })}
                                      className="w-full p-4 text-sm text-gray-700 hover:ring-blue-200 ring-2 ring-gray-100 bg-white border rounded-xl border-white focus:ring-blue-300 focus:border-blue-300 block transition duration-150 ease-in-out"
                                    />
                                    <p className='text-[12px]  text-gray-500'>You can change your description</p>
                                  </div>
                                </div>

                              </div>
                              <Modal.Footer className='flex flex-row justify-between  h-fit items-center '>
                                <div className='flex flex-row justify-center items-center gap-2 '>
                                  <button className="px-3 p-2 rounded-full text-nowrap w-auto bg-blue-500 text-white border-0 outline-none hover:bg-blue-400"
                                    onClick={() => {
                                      handleEditTodo(editedBlog)
                                      handleEditTopicSuccess();
                                      onCloseTask()
                                    }}>
                                    Save Changes
                                  </button>
                                  <button
                                    className="px-3 p-2 rounded-full bg-red-500 text-white border-0 outline-none hover:bg-red-400"
                                    onClick={onCloseTask}
                                  >
                                    Close
                                  </button>
                                </div>
                                <button
                                  className="px-3 p-2 rounded-full  bg-red-600 text-white border-0 outline-none hover:bg-red-800"
                                  onClick={() => {
                                    const selectedTopic = topics[selectedBlogIndex]
                                    if (selectedTopic && selectedTopic.id) {
                                      handleRemoveTodo(selectedTopic.id);
                                      handleDeleteTopicSuccess();
                                      onCloseTask()

                                    } else {
                                      console.error("Error: Selected topic object is invalid or missing id");
                                    }
                                  }}
                                >
                                  Remove
                                </button>
                              </Modal.Footer>
                            </div>
                          </Modal.Body>
                        </Modal>
                      </div>
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              ))}
            </DragDropContext>
            {/* Modal */}
            <div className="modalOuterContainer">
              <Modal show={openModal} onClose={() => { onCloseModal(true) }} size="md" popup>
                <Modal.Header />
                <Modal.Body>
                  <div className="space-y-6">
                    <h1 className='text-xl font-medium text-gray-900 mb-3 dark:text-white'>Type</h1>
                    <div className="text-xl font-medium text-gray-500 dark:text-white">

                      <div className="max-w-md pl-5 break-all">{type}</div>
                    </div>
                    <div className="max-w-md">
                      <div className="mb-2 block">

                      </div>
                      <h1 className='text-xl font-medium text-gray-900 mb-3 dark:text-white'>Title</h1>
                      <select
                        
                        id="title"
                        name="title"
                        required
                        value={title}
                        onChange={(e: any) => {
                          setTitle(e.target.value)
                        }}
                        className="w-full bg-blue-300 mb-10 p-4 px-6 text-[20px]  shadow-lg shadow-gray-300 max-w-xs text-white outline-white rounded-2xl focus:outline-none focus:ring-0 border-none hover:border-none"
                      >

                        <option disabled selected value="">Select your title</option>
                        <option value="Banner">Banner</option>
                        <option value="Illustration">Illustration</option>
                        <option value="Video">Video</option>
                        <option value="UI UX">UI UX</option>

                      </select>
                      <p className='text-[12px]  text-gray-500 pb-2'>You can write your title</p>
                      <input type="text" value={title} onChange={(e: any) => {
                        setTitle(e.target.value)
                      }}
                        className="w-fit p-4 text-sm text-gray-700 hover:ring-blue-200 ring-2 ring-gray-100 bg-white border rounded-xl border-white focus:ring-blue-300 focus:border-blue-300 block transition duration-150 ease-in-out"
                      />

                      <h1 className='text-xl font-medium mb-3 text-gray-900 dark:text-white'>Description</h1>
                      <textarea
                        id="description"
                        placeholder=""
                        value={description}
                        onChange={(e: any) => {
                          setDescription(e.target.value)
                        }}
                        className="w-full p-4 hover:ring-blue-300 ring-2 ring-gray-100   hover:ring-2 text-sm text-gray-700 bg-white border rounded-xl border-white focus:ring-blue-300 focus:border-blue-300 block transition duration-150 ease-in-out"
                      />
                    </div>
                    <div className='flex flex-row justify-end items-end align-bottom  w-full gap-2'>
                      <button
                        onClick={() => {
                          if (!title || !description) {

                          } else {
                            handleAddTodo({
                              id,
                              userId,
                              title,
                              type,
                              description
                            });
                            handleAddTopicSuccess();
                            onCloseModal(true);

                          }
                        }}
                        type='submit'
                        className="py-2 px-4 font-semibold w-fit text-[16px] rounded-3xl shadow-md shadow-gray-300 bg-blue-600 text-white border-0 outline-none hover:bg-blue-400"
                      >
                        Add
                      </button>
                      <button
                        className="py-2 px-4 font-semibold w-fit text-[16px] rounded-3xl shadow-md shadow-gray-300 bg-red-500 text-white border-0 outline-none hover:bg-red-400"
                        onClick={onCloseModal}>
                        Close
                      </button>

                      <button
                        className="px-3 p-2 rounded-full bg-red-600 text-white border-0 outline-none hover:bg-red-800"
                        onClick={() => {
                          setOpenConfirm(true)
                        }}
                      >
                        Remove
                      </button>


                      <Modal show={openConfirm} size="md" onClose={onCloseConfirm} popup>
                        <Modal.Header />
                        <Modal.Body className='text-gray-700'>
                          <div className="flex flex-col justify-center items-start gap-5">
                            <h1 className='text-red-500 text-xl'>Warning!</h1>
                            <p className='text-md '>Are you sure you want to delete your task? You will not be able to recover this task and related data.</p>
                            <p className='text-gray-500 text-[12px]'>Please confirm your password to delete your task.</p>
                            <TextInput
                              id="deletePassword"
                              type="password"
                              placeholder="Password"
                              value={password}
                              onChange={handlecheckPassword}
                              required
                              className='w-full pr-2'

                            />
                            <div className='flex justify-end w-full pr-3 gap-3'>
                              <button
                                className='bg-red-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white'
                                onClick={() => {

                                  if (password === '') {
                                    alert('Please enter your password')
                                  } else {
                                    handleDeleteColumn();
                                    onCloseConfirm;
                                    onCloseModal(true);
                                  }
                                }}>Delete</button>
                              <button
                                className='bg-blue-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white'
                                onClick={() => { onCloseConfirm() }}>Cancel</button>
                            </div>
                          </div>
                        </Modal.Body>
                      </Modal>

                    </div>

                  </div>
                  <p className='text-[12px] mt-2 text-gray-400 font-sans m-0 w-full items-start h-fit px-2 justify-end flex'>Delete task</p>

                </Modal.Body>
              </Modal>

            </div>
          </ScrollShadow>
        </div>

      </div>
    </div>
  );
};

export default ActiveSprint;
