"use client"

import React, { useContext, useState } from 'react';
import { CiSearch } from 'react-icons/ci';
import { FaPlus } from 'react-icons/fa';
import { MdOutlineReadMore } from "react-icons/md";
import { CreateModal } from './CreateModal';
import { Modal } from 'flowbite-react';
import { Context } from '../../contexts/TopicContext';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { DragDropContext, Droppable } from '@hello-pangea/dnd';
import { Footer } from "flowbite-react";
import { BsDribbble, BsFacebook, BsGithub, BsInstagram, BsTwitter } from "react-icons/bs";
import "../../globals.css";
import Loader from '../Loader';

const ActiveSprint = () => {
    const [showBacklog, setShowBacklog] = useState(false);
    const [showTodo, setShowTodo] = useState(false);
    const [showInprog, setShowInprog] = useState(false);
    const [showComplete, setShowComplete] = useState(false);


    const {
        SignupSchema, onCloseModal, topic, setOpenModal, openModal, loading, handleAddTodo, title, id, userId, topics, setTitle, editedBlog,
        description, setDescription, setType, type, handleDragEnd
    } = useContext(Context);

    return (

        <div className='w-auto  max-w-[900px] min-h-screen  h-full bg-white flex flex-col justify-between items-center'>
            {loading && <Loader />}
            <h1 className='text-black text-xl font-semibold mt-4'>#Design Ops</h1>
            {/* Search */}
            <div className='flex flex-row justify-between align-bottom w-full h-20 '>
                <p className='text-gray-600 text-xs flex items-center justify-center'>Active Sprint</p>
                <div className='bg-inherit w-[100px] border-b border-slate-300 h-1/2 mt-6 align-bottom justify-center items-center flex'>
                    <input name='search' className='bg-inherit text-[10px] text-gray-600 outline-none w-auto max-w-[100px]  min-w-[80px]' placeholder="Search..." />
                    <button className='w-[18px] h-[18px] text-gray-400'>
                        <CiSearch className='w-full h-full' />
                    </button>
                </div>
            </div>
            {/* Main */}
            <div className='w-full h-screen mt-1 bg-purple-50 max-h-[400px]   rounded-[40px] items-center align-middle flex flex-col'>
                {/* Create task and friends */}
                <div className='flex flex-row justify-center gap-10 items-center  mt-5 mb-3 h-[60px] w-full'>
                    <button className="min-w-[70px] w-[100px] shadow-lg transition duration-300 ease-in-out text-[10px] font-semibold h-auto py-[5px] shadow-gray-300 rounded-2xl bg-white text-black border-0 hover:bg-blue-200 hover:text-blue-600">
                        Invite People
                    </button>
                    <div className='flex justify-center items-center'>
                        <CreateModal />
                    </div>
                </div>
                <div className='flex w-full h-auto p-0 justify-center  max-h-[800px] min-x-[200px] bg-indigo-50'>
                    <div className='overflow-scroll gap-5 flex-col flex justify-evenly w-[95%]'>

                        <div id='Backlog' className='flex flex-row justify-center gap-6 items-center'>
                            <button className="shadow-gray-300 h-[42px] shadow-md text-black  border-0 max-w-[250px] text-base w-11/12 font-semibold pl-4 rounded-[8px] items-center flex justify-between transition duration-300 ease-in-out bg-white hover:bg-blue-200 hover:text-blue-600"
                                onClick={() => {
                                    setOpenModal(true);
                                    setType('Backlog');
                                }}>
                                Backlog
                                <div className='bg-blue-200 text-blue-400 rounded-full w-[25px] h-[25px] mr-3 flex items-center justify-center'>
                                    <FaPlus className="h-[13px] w-[13px]" />
                                </div>
                            </button>
                            <button
                                onClick={() => setShowBacklog(true)}
                                className='w-[40px] h-[40px] bg-white text-[20px] border-2 hover:border-blue-400 hover:bg-blue-300 hover:text-white border-blue-300 rounded-full shadow-md shadow-gray-200 text-gray-400 flex justify-center items-center '>
                                <MdOutlineReadMore />
                            </button>
                        </div>


                        <div id='To Do' className='flex flex-row justify-center gap-6 items-center'>
                            <button className="button-container shadow-gray-300 h-[42px] max-w-[250px] shadow-md text-black border-0 text-base w-11/12 font-semibold pl-4 rounded-[8px] items-center flex justify-between transition duration-300 ease-in-out bg-white hover:bg-blue-200 hover:text-blue-600"
                                onClick={() => {
                                    setOpenModal(true);
                                    setType('To Do');
                                }}>
                                To Do
                                <div className='bg-blue-200 rounded-full w-[25px] h-[25px] mr-3 flex items-center justify-center'>
                                    <FaPlus className="text-blue-400 h-[13px] w-[13px]" />
                                </div>
                            </button>
                            <button
                                onClick={() => setShowTodo(true)}
                                className='w-[40px] h-[40px] bg-white text-[20px] border-2 hover:border-blue-400 hover:bg-blue-300 hover:text-white border-blue-300 rounded-full shadow-md shadow-gray-200 text-gray-400 flex justify-center items-center '>
                                <MdOutlineReadMore />
                            </button>
                        </div>


                        <div id='In Progress' className='flex flex-row justify-center gap-6 items-center'>
                            <button className="button-container shadow-gray-300 h-[42px] max-w-[250px] shadow-md text-black border-0 text-base w-11/12 font-semibold pl-4 rounded-[8px] items-center flex justify-between transition duration-300 ease-in-out bg-white hover:bg-blue-200 hover:text-blue-600"
                                onClick={() => {
                                    setOpenModal(true);
                                    setType('In Progress');
                                }}>
                                In Progress
                                <div className='bg-blue-200 rounded-full w-[25px] h-[25px] mr-3 flex items-center justify-center'>
                                    <FaPlus className="text-blue-400 h-[13px] w-[13px]" />
                                </div>
                            </button>
                            <button
                                onClick={() => setShowInprog(true)}
                                className='w-[40px] h-[40px] bg-white text-[20px] border-2 hover:border-blue-400 hover:bg-blue-300 hover:text-white border-blue-300 rounded-full shadow-md shadow-gray-200 text-gray-400 flex justify-center items-center '>
                                <MdOutlineReadMore />
                            </button>
                        </div>



                        <div id='Completed' className='flex flex-row justify-center gap-6 items-center'>
                            <button className="button-container shadow-gray-300 h-[42px] shadow-md text-black border-0 max-w-[250px] text-base w-11/12 font-semibold pl-4 rounded-[8px] items-center flex justify-between transition duration-300 ease-in-out bg-white hover:bg-blue-200 hover:text-blue-600"
                                onClick={() => {
                                    setOpenModal(true);
                                    setType('Completed');
                                }}>
                                Completed
                                <div className='bg-blue-200 rounded-full w-[25px] h-[25px] mr-3 flex items-center justify-center'>
                                    <FaPlus className="text-blue-400 h-[13px] w-[13px]" />
                                </div>
                            </button>
                            <button
                                onClick={() => setShowComplete(true)}
                                className='w-[40px] h-[40px] bg-white text-[20px] border-2 hover:border-blue-400 hover:bg-blue-300 hover:text-white border-blue-300 rounded-full shadow-md shadow-gray-200 text-gray-400 flex justify-center items-center '>
                                <MdOutlineReadMore />
                            </button>
                        </div>





                        {showBacklog ? (
                            <>
                                <div className='w-fit h-fit flex-col justify-center fixed items-center align-middle flex inset-0 z-50 bg-white shadow-md shadow-gray-300 m-auto rounded-lg'>
                                    <h1 className='text-black text-[20px] mt-10'>Backlog</h1>
                                    <div className='flex-row flex flex-wrap p-3'>
                                        <DragDropContext onDragEnd={handleDragEnd}>

                                            <Droppable droppableId="Backlog">
                                                {(provided) => (
                                                    <div {...provided.droppableProps} ref={provided.innerRef} className='flex flex-row  h-[300px] w-[300px] align-middle overflow-scroll overflow-y-auto pt-10 items-center transition-all duration-300'>
                                                        {/* <CardTopic /> */}
                                                    </div>
                                                )}
                                            </Droppable>
                                        </DragDropContext>
                                    </div>
                                    <div className='flex justify-center items-center w-full h-full'>
                                        <button
                                            className="p-2 w-[20%] mb-2 rounded-2xl bg-red-500 text-white border-0 outline-none hover:bg-red-400"
                                            type="button"
                                            onClick={() => setShowBacklog(false)}
                                        >close
                                        </button>
                                    </div>

                                </div>

                            </>
                        ) : null}
                        {showTodo ? (
                            <>
                                <div className='w-fit h-fit flex-col justify-center fixed items-center align-middle flex inset-0 z-50 bg-white shadow-md shadow-gray-300 m-auto rounded-lg'>
                                    <h1 className='text-black text-[20px] mt-10'>To Do</h1>

                                    <div className='flex-row flex flex-wrap p-3'>
                                        <DragDropContext onDragEnd={handleDragEnd}>

                                            <Droppable droppableId="To Do">
                                                {(provided) => (
                                                    <div {...provided.droppableProps} ref={provided.innerRef} className='flex flex-row  h-[300px] w-[300px] align-middle overflow-scroll overflow-y-auto pt-10 items-center transition-all duration-300'>
                                                        {/* <CardToDo /> */}
                                                    </div>
                                                )}
                                            </Droppable>
                                        </DragDropContext>
                                    </div>
                                    <div className='flex justify-center items-center w-full h-full'>
                                        <button
                                            className="p-2 w-[20%] mb-2 rounded-2xl bg-red-500 text-white border-0 outline-none hover:bg-red-400"
                                            type="button"
                                            onClick={() => setShowTodo(false)}
                                        >close
                                        </button>
                                    </div>

                                </div>
                            </>
                        ) : null}
                        {showInprog ? (
                            <>
                                <div className='w-fit h-fit flex-col justify-center fixed items-center align-middle flex inset-0 z-50 bg-white shadow-md shadow-gray-300 m-auto rounded-lg'>
                                    <h1 className='text-black text-[20px] mt-10'>In Progress</h1>

                                    <div className='flex-row flex flex-wrap p-3'>
                                        <DragDropContext onDragEnd={handleDragEnd}>

                                            <Droppable droppableId="In Progressp">
                                                {(provided) => (
                                                    <div {...provided.droppableProps} ref={provided.innerRef} className='flex flex-row  h-[300px] w-[300px] align-middle overflow-scroll overflow-y-auto pt-10 items-center transition-all duration-300'>
                                                        {/* <CardInProg /> */}
                                                    </div>
                                                )}
                                            </Droppable>
                                        </DragDropContext>
                                    </div>
                                    <div className='flex justify-center items-center w-full h-full'>
                                        <button
                                            className="p-2 w-[20%] mb-2 rounded-2xl bg-red-500 text-white border-0 outline-none hover:bg-red-400"
                                            type="button"
                                            onClick={() => setShowInprog(false)}
                                        >close
                                        </button>
                                    </div>

                                </div>
                            </>
                        ) : null}
                        {showComplete ? (
                            <>
                                <div className='w-fit h-fit flex-col justify-center fixed items-center align-middle flex inset-0 z-50 bg-white shadow-md shadow-gray-300 m-auto rounded-lg'>
                                    <h1 className='text-black text-[20px] mt-10'>Completed</h1>

                                    <div className='flex-row flex flex-wrap p-3'>
                                        <DragDropContext onDragEnd={handleDragEnd}>

                                            <Droppable droppableId="Completed">
                                                {(provided) => (
                                                    <div {...provided.droppableProps} ref={provided.innerRef} className='flex flex-row  h-[300px] w-[300px] align-middle overflow-scroll overflow-y-auto pt-10 items-center transition-all duration-300'>
                                                        {/* <CardComplete /> */}
                                                    </div>
                                                )}
                                            </Droppable>
                                        </DragDropContext>
                                    </div>
                                    <div className='flex justify-center items-center w-full h-full'>
                                        <button
                                            className="p-2 w-[20%] mb-2 rounded-2xl bg-red-500 text-white border-0 outline-none hover:bg-red-400"
                                            type="button"
                                            onClick={() => setShowComplete(false)}
                                        >close
                                        </button>
                                    </div>

                                </div>
                            </>
                        ) : null}


                        <div className="modalOuterContainer">
                            <Formik
                                enableReinitialize={true}
                                initialValues={{
                                    type: type,
                                    title: '',
                                    description: '',
                                }}
                                validationSchema={SignupSchema}
                                onSubmit={(values, { setSubmitting }) => {
                                    try {
                                        const newTodo = {
                                            id,
                                            userId,
                                            type,
                                            title: values.title,
                                            description: values.description,

                                        };
                                        setSubmitting(false);
                                        handleAddTodo(newTodo);
                                        onCloseModal(true);
                                    } catch (error) {
                                        console.error('Error adding todo:', error);
                                    }

                                }}>

                                <Form>
                                    <Modal show={openModal} onClose={() => { onCloseModal(true) }} size="md" popup>
                                        <Modal.Header />
                                        <Modal.Body>
                                            <div className="space-y-6">
                                                <h1 className='text-xl font-medium text-gray-900 mb-3 dark:text-white'>Type</h1>
                                                <div className="text-xl font-medium text-gray-500 dark:text-white">
                                                    <div className="max-w-md pl-5">{type}</div>
                                                </div>
                                                <div className="max-w-md">
                                                    <div className="mb-2 block">

                                                    </div>
                                                    <h1 className='text-xl font-medium text-gray-900 mb-3 dark:text-white'>Title</h1>
                                                    <Field
                                                        as="select"
                                                        id="title"
                                                        name="title"
                                                        required
                                                        value={title}
                                                        onChange={(e: any) => {
                                                            setTitle(e.target.value)
                                                        }}
                                                        className="w-full bg-blue-300 mb-10 p-4 px-6 text-[20px]  shadow-lg shadow-gray-300 max-w-xs text-white outline-white rounded-2xl focus:outline-none focus:ring-0 border-none hover:border-none"
                                                    >

                                                        <option disabled selected value="">Select your title</option>
                                                        <option value="Banner">Banner</option>
                                                        <option value="Illustration">Illustration</option>
                                                        <option value="Video">Video</option>
                                                        <option value="UI UX">UI UX</option>

                                                    </Field>

                                                    <h1 className='text-xl font-medium mb-3 text-gray-900 dark:text-white'>Description</h1>
                                                    <Field
                                                        as="textarea"

                                                        id="description"
                                                        placeholder=""
                                                        value={description}
                                                        onChange={(e: any) => {
                                                            setDescription(e.target.value)
                                                        }}
                                                        className="w-full p-4 hover:ring-blue-300 ring-2 ring-gray-100   hover:ring-2 text-sm text-gray-700 bg-white border rounded-xl border-white focus:ring-blue-300 focus:border-blue-300 block transition duration-150 ease-in-out"

                                                    />


                                                </div>
                                                <div className='flex flex-row justify-end items-center w-full gap-2'>
                                                    <button
                                                        onClick={() => {
                                                            if (!title || !description) {

                                                            } else {
                                                                handleAddTodo({
                                                                    id,
                                                                    userId,
                                                                    title,
                                                                    type,
                                                                    description
                                                                });
                                                                onCloseModal(true);

                                                            }
                                                        }}
                                                        type='submit'
                                                        className="p-2 w-[20%] rounded-2xl bg-blue-500 text-white border-0 outline-none hover:bg-blue-400"
                                                    >
                                                        Add
                                                    </button>
                                                    <button
                                                        className="p-2 w-[20%] rounded-2xl bg-red-500 text-white border-0 outline-none hover:bg-red-400"
                                                        onClick={onCloseModal}>
                                                        Close
                                                    </button>
                                                </div>
                                            </div>
                                        </Modal.Body>
                                    </Modal>
                                </Form>
                            </Formik>
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-full pt-10 bg-white text-[12px] flex justify-center items-center">
                <Footer container className="rounded-none w-full h-[200px]">
                    <div className="w-full  justify-center">
                        <div className="flex flex-row w-full justify-between items-center gap-2 ">

                            <div className='flex justify-center w-[70px] h-[70px]  items-center  bg-blue-400 rounded-full border-blue-700  border-solid border-[4px]'>
                                <h1 className='flex justify-center w-full h-full text-[30px]  font-semibold items-center text-blue-white p-2 rounded-full'>
                                    DO
                                </h1>
                            </div>


                            <div className="grid grid-cols-2 text-[10px] gap-8 sm:mt-4 sm:grid-cols-3 sm:gap-6">
                                <div>
                                    <Footer.Title title="about" />
                                    <Footer.LinkGroup col>
                                        <Footer.Link href="#">Flowbite</Footer.Link>
                                        <Footer.Link href="#">Tailwind CSS</Footer.Link>
                                    </Footer.LinkGroup>
                                </div>
                                <div>
                                    <Footer.Title title="Follow us" />
                                    <Footer.LinkGroup col>
                                        <Footer.Link href="#">Github</Footer.Link>
                                        <Footer.Link href="#">Discord</Footer.Link>
                                    </Footer.LinkGroup>
                                </div>

                            </div>
                        </div>
                        <Footer.Divider />
                        <div className=" important:text-[10px] flex flex-row w-full gap-10 sm:flex sm:items-center m-0 sm:justify-between">
                            <Footer.Copyright href="#" by="Design Ops™" year={2024} />
                            <div className="flex gap-2 sm:mt-0 sm:justify-center items-center">
                                <Footer.Icon href="#" icon={BsFacebook} />
                                <Footer.Icon href="#" icon={BsInstagram} />

                            </div>
                        </div>
                    </div>
                </Footer>
            </div>
        </div>

    );
};

ActiveSprint.getInitialProps = async () => {
    return {};
};

export default ActiveSprint;
