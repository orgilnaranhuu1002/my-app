'use client'

import React, { useContext, useEffect, useState } from 'react';
import { Alert, Label, Modal } from 'flowbite-react';
import { HiPlus } from 'react-icons/hi';
import { Context } from '@/app/contexts/TopicContext';
import {
  Button,
  Form,
  Input,
} from 'antd';
export function CreateModal() {
  const {
    handleAddColumn,
    name,
    setName,
    userId,
    id
  } = useContext(Context);
  const [openModal, setOpenModal] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [alertColor, setAlertColor] = useState('success');

  const [form] = Form.useForm();

  const handleSubmit = () => {
    form.validateFields()
      .then(values => {
        const { columnName } = values;
        handleAddColumn({ userId, id, name });
        onCloseModal();
        handleAddColumnSuccess();
        setAlertColor('success');
        setShowAlert(true);
        setName('');
      })
      .catch(() => {
        setAlertMessage('Please input!');
        setAlertColor('error');
        setShowAlert(true);
      });
  };
  const onCloseModal = () => {
    setOpenModal(false);
    setName('');
  };
  const handleAddColumnSuccess = () => {
    setAlertMessage('Task added successfully!');
    setAlertColor('success');
    setShowAlert(true);
  };
  useEffect(() => {
    if (showAlert) {
      const timer = setTimeout(() => {
        setShowAlert(false);
      }, 3000);

      return () => clearTimeout(timer);
    }
  }, [showAlert]);

  return (
    <>
      <button
        onClick={() => setOpenModal(true)}
        className="flex flex-row items-center justify-between px-3 h-auto py-[10px] gap-2 shadow-md shadow-gray-400 max-w-[160px] min-w-[50px] w-full bg-blue-600 rounded-3xl"
      >
        <p className="text-nowrap text-sm font-semibold text-gray-200">
          Create Task
        </p>
        <div className="bg-blue-400 rounded-full w-[26px] h-[26px] flex items-center justify-center">
          <HiPlus className="w-[15px] h-[16px] text-gray-200" />
        </div>
      </button>
      {/* Modal */}
      <Modal show={openModal} size="md" onClose={onCloseModal} popup>
        <Modal.Header />
        <Modal.Body>
          <Form
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 14 }}
            layout="horizontal"
            form={form}
            style={{ maxWidth: 600 }}
          >
            <div className="gap-3 flex flex-col ">
              <div className="max-w-md transition-all duration-300">
                <h1 className='text-xl font-medium mb-3 text-gray-900 dark:text-white'>Task name</h1>

                <Form.Item name={'columnName'} rules={[{ required: true, message: 'Please input!' }]}>
                  <Input
                    value={name}
                    name='columnName'
                    placeholder="Task name"
                    className="w-full p-4 hover:ring-blue-300 ring-2 text-gray-700   ring-gray-100 mb-2  hover:ring-2 text-sm  bg-white border rounded-xl border-white focus:ring-blue-300 focus:border-blue-300 block transition duration-150 ease-in-out"
                    onChange={(e: any) => {
                      setName(e.target.value)
                    }}
                  />
                </Form.Item>
                <p className='text-gray-500 text-[12px]'>Write your task name</p>
              </div>
              <div className='flex flex-row gap-2 justify-end'>
                <Button
                  htmlType="submit"
                  className=" font-semibold w-fit rounded-3xl shadow-md shadow-gray-300 bg-blue-600 text-white border-0 outline-none hover:bg-blue-400"
                  onClick={() => {
                    handleSubmit();
                    onCloseModal();
                    handleAddColumnSuccess();
                  }}
                >
                  Add
                </Button>
                <Button
                  className=" w-fit font-semibold rounded-3xl shadow-md shadow-gray-300 bg-red-500 text-white border-0 outline-none hover:bg-red-400"
                  onClick={onCloseModal}>
                  Close
                </Button>
              </div>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {showAlert && (
        <div className="fixed  z-50 top-5 left-1/2 w-[400px]  transform -translate-x-1/2">
          <Alert
            color={alertColor}
            onDismiss={() => setShowAlert(false)}
            className='text-center shadow-xl shadow-gray-100 '

          >
            {alertMessage}
          </Alert>
        </div>
      )}
    </>
  );
}


