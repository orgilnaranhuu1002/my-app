"use client"

import React, { useContext, useState } from 'react';
import { FaRegBell } from 'react-icons/fa';
import { IoSettingsOutline } from 'react-icons/io5';
import { RxQuestionMarkCircled } from 'react-icons/rx';
import Image from 'next/image';
import '../../globals.css';
import emptyprofile from '../../public/src/userProfilePic.jpeg'
import { Calendar } from "@nextui-org/react";
import { today, getLocalTimeZone, isWeekend } from "@internationalized/date";
import { useLocale } from "@react-aria/i18n";
import { Dropdown, DropdownTrigger, DropdownMenu, DropdownItem, Button } from "@nextui-org/react";
import Loader from '../Loader';
import Link from 'next/link';
import { Context } from '@/app/contexts/TopicContext';




const More = () => {
  const { topics, getBackgroundColor } = useContext(Context);
  const [loading, setLoading] = React.useState(false);
  let userString;
  if (typeof window !== "undefined") {
    userString = localStorage.getItem('User');
  }
  const storedUser = userString ? JSON.parse(userString) : null;
  const username = storedUser ? storedUser.username : null;
  const profileImage = storedUser ? storedUser.profileImage : null;
  let [date, setDate] = React.useState(today(getLocalTimeZone()));
  let { locale } = useLocale();



  const getCookie = (name: any) => {
    console.log("Attempting to get cookie:", name);
    const token = getCookie('token');
    console.log("Token from cookie:", token);
    let cookie: { [key: string]: string } = {};
    console.log("Before splitting cookie string");
    document.cookie.split(';').forEach(function (el) {
      console.log("Parsing cookie string:", el);
      let [k, v] = el.split('=');
      console.log("Cookie parsed:", k.trim(), v);
      cookie[k.trim()] = v;
    });
    console.log("Cookie object:", cookie);
    if (!cookie[name]) {
      console.log("Cookie not found");
      return null;
    }
    console.log("Cookie found:", cookie[name]);
    return cookie[name];

  };

  const deleteCookie = (name: string) => {
    console.log('Deleting cookie:', name);
    document.cookie = `${name}=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT`;
  };


  const logout = () => {
    try {
      setLoading(true);
      window.localStorage.removeItem('User');
      deleteCookie('token');
    } catch (error) {
      console.error('Error logging out:', error);
    } finally {
      window.location.reload();
      setLoading(false);
    }
  };
  let defaultDate = today(getLocalTimeZone());

  return (

    <div className='w-[300px]  max-w-[400px] px-6  flex justify-center bg-white flex-col h-fit  items-center'>
      {loading && <Loader />}
      <div className='w-full  min-h-[600px] h-fit '>
        <div className='flex flex-row w-full h-[50px] justify-end gap-2 items-center mb-[30px] pr-4 '>
          <button className='rounded-full bg-gray-100 h-[25px] w-[25px] border-2 border-gray-200 flex justify-center items-center'>
            <FaRegBell className=' w-[11px] h-[13px] text-blue-500 flex justify-center items-center z ' />
          </button>
          <button className='rounded-full bg-gray-100 h-[25px] w-[25px] border-2 border-gray-200 flex justify-center items-center'>
            <RxQuestionMarkCircled className='w-[15px] h-[15px] text-blue-500 ' />
          </button>
          <button className='rounded-full bg-gray-100 h-[25px] w-[25px] border-2 border-gray-200 flex justify-center items-center'>
            <IoSettingsOutline className='w-[15px] h-[15px] text-blue-500 ' />
          </button>
        </div>


        <div className="flex flex-row w-full justify-center items-center bg-white">
          <div className='w-11/12 flex flex-row justify-between items-center bg-white '>
            <div className="flex flex-col h-fit  ">
              <div className=" text-lg font-bold capitalize text-wrap break-all h-fit  text-black">Hi,  {username}</div>
              <p className='text-gray-800 text-xs flex-nowrap'>
                Today is {new Date().toLocaleDateString('en-US', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })}
              </p>
            </div>
            <Dropdown>
              <DropdownTrigger>
                <Button
                  variant="bordered"
                >
                  <Image
                    className=' h-[40px] bordered border-2 border-gray-500 m-2 max-w-[40px] rounded-full'
                    src={profileImage || emptyprofile}
                    alt='Profile'
                    width={100}
                    height={100}
                  />
                </Button>
              </DropdownTrigger>
              <DropdownMenu aria-label="Static Actions" className='flex items-center justify-center bg-white rounded-lg shadow-lg shadow-gray-400 text-black w-[100px] h-[100px]'>
                <DropdownItem key="new" className='capitalize text-black'><Link href={'/profile'}>{username}</Link></DropdownItem>
                <DropdownItem key="copy"></DropdownItem>

                <DropdownItem key="delete" className="text-red-500" href='/login' onClick={logout}>

                  Log out

                </DropdownItem>

              </DropdownMenu>
            </Dropdown>

          </div>
        </div>


        <div className=' w-full flex flex-col mt-7  justify-center items-center  text-black  '>
          <div className='bg-[#f0f5ff]  rounded-3xl'>
            <Calendar
              aria-label="Date (Invalid on weekends)"
              value={date}
              onChange={setDate}
              className='w-[260px]  flex justify-center text-[14px]  items-center h-fit'
            />
          </div>

          <div className=' overflow-auto min-h-[200px]  max-h-[400px] w-full '>
            <h1 className='button-container  text-black font-semibold text-lg pl-2 py-3 items-start bg-white  '>
              Upcoming Task
            </h1>

            <ul className='max-h-[400px] w-full flex flex-col  gap-3'>

              {topics ? (
                (topics || []).map((topic, index) => (
                  <div key={index} className={`text-gray-700 h-auto flex flex-col gap-2 p-4 mb-5 rounded-2xl text-xs bg-${getBackgroundColor(topic.title)}`}>
                    <div className='flex flex-row justify-between w-[98%]'>
                      <h1 className='font-semibold text-[13px]'>{topic.title}</h1>
                      <p className='text-gray-500 text-[10px]'>{topic.type}</p>
                    </div>
                    <p className='w-[80%] md:break-words'>{topic.description}</p>
                  </div>
                ))) : (
                <p>No topics available</p>
              )}
            </ul>
          </div>


        </div>
      </div>
    </div>

  )
}

export default More