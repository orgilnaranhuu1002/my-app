import React, { useState } from 'react';
import styled from 'styled-components';
import { BsPlus } from 'react-icons/bs';
import Image from 'next/image'
import profile from '../../public/src/girl 1.jpg'
import { FaRegBell, FaRegCalendarAlt } from 'react-icons/fa';
import { GrTasks } from 'react-icons/gr';
import { IoSettingsOutline } from 'react-icons/io5';
import { RxQuestionMarkCircled } from 'react-icons/rx';
import { Button, Dropdown, DropdownItem, DropdownMenu, DropdownTrigger } from '@nextui-org/react';

const FloatingButton = () => {
    const [isOpen, setIsOpen] = useState(false);
    const toggleButtons = () => {
        setIsOpen(!isOpen);
    };

    const [loading, setLoading] = React.useState(false);
    const userString = window.localStorage.getItem('User');
    const storedUser = userString ? JSON.parse(userString) : null;
    const username = storedUser ? storedUser.username : null;
    const logout = () => {
        try {
            setLoading(true);
            window.localStorage.removeItem('User');
        } catch (error) {
            console.error('Error logging out:', error);
        } finally {
            window.location.reload();
            setLoading(false);
        }

    };

    return (
        <Container>
            {isOpen && (
                <>
                    <div className='flex !z-50  justify-center items-center ring-1 bg-indigo-100 w-[10px] rounded-full  '>
                        <div className='flex justify-center flex-col items-center gap-5 p-2'>
                            <div className="relative group inline-block">




                                <Dropdown>
                                    <DropdownTrigger>
                                        <Button
                                            variant="bordered"
                                        >
                                            <Image
                                                className=' h-[40px] max-w-[40px] rounded-full'
                                                src={profile}
                                                alt=''
                                            />
                                        </Button>
                                    </DropdownTrigger>
                                    <DropdownMenu aria-label="Static Actions" className='flex items-center justify-center bg-white rounded-lg shadow-lg shadow-gray-400 text-black w-[100px] h-[100px]'>

                                        <DropdownItem key="copy" className='flex justify-center items-center'>{username}</DropdownItem>

                                        <DropdownItem key="delete" className="text-red-500 flex justify-center items-center" href='/login' onClick={logout}>

                                            Log out

                                        </DropdownItem>

                                    </DropdownMenu>
                                </Dropdown>
                                <div className="absolute text-black right-12 bottom-12 p-2 w-[100px] text-[14px] font-sans bg-white border border-gray-300 rounded shadow-lg opacity-0 group-hover:opacity-100 transition-opacity duration-300">
                                    <p className='text-wrap break-all'>Hi, {username}</p>
                                </div>
                            </div>
                            <div className='flex flex-col w-[50px] h-auto mt-1  justify-center  gap-1 items-center '>
                                <button className='rounded-full bg-gray-100 h-[20px] w-[20px] border-[1px] border-gray-300 flex justify-center items-center'>
                                    <FaRegBell className=' w-[11px] h-[13px] text-blue-500 flex justify-center items-center z ' />
                                </button>
                                <button className='rounded-full bg-gray-100  h-[20px] w-[20px] border-[1px] border-gray-300 flex justify-center items-center'>
                                    <RxQuestionMarkCircled className='w-[15px] h-[15px] text-blue-500 ' />
                                </button>
                                <button className='rounded-full bg-gray-100 h-[20px] w-[20px] border-[1px] border-gray-300 flex justify-center items-center'>
                                    <IoSettingsOutline className='w-[15px]  h-[15px] text-blue-500 ' />
                                </button>
                            </div>
                            <div className='text-purple-300  flex shadow-md shadow-gray-400  items-center  flex-col w-[34px] h-[80px]  bg-indigo-500 rounded-full '>
                                <div className='py-2 flex justify-between h-full w-full flex-col items-center'>
                                    <div className="relative group inline-block">
                                        <button className='fcol bg-white ring-1 h-[25px] w-[25px] ring-blue-700 rounded-full'>

                                            <FaRegCalendarAlt className='w-full h-full p-[1px] rounded-full' />
                                        </button>
                                        <div className="absolute text-black right-6 bottom-6 p-1  w-[60px] h-auto text-[12px] font-sans bg-gray-50 border  border-gray-300 rounded shadow-lg opacity-0 group-hover:opacity-100 transition-opacity duration-300">
                                            Calendar
                                        </div>
                                    </div>
                                    <div className="relative group inline-block">

                                        <button className='bg-white fcol ring-1 h-[25px] w-[25px] ring-blue-700 rounded-full'>

                                            <GrTasks className='w-full h-full p-[1px] rounded-full' />
                                        </button>
                                        <div className="absolute text-black right-6 bottom-6 p-1  w-[80px] h-auto text-[12px] font-sans bg-gray-50 border  border-gray-300 rounded shadow-lg opacity-0 group-hover:opacity-100 transition-opacity duration-300">
                                            Up coming tasks
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </>
            )}
            <MainButton onClick={toggleButtons}>
                <BsPlus size={24} />
            </MainButton>
        </Container>
    );
};

const Container = styled.div`
    position: fixed;
    bottom: 20px;
    right: 20px;
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 10px;
    z-index:50;
`;

const MainButton = styled.button`
    width: 50px;
    height: 50px;
    background-color:rgb(104 117 245);
    color: white;
    border:rgb(144 97 249 ) solid 2px;

    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
    cursor: pointer;
    transition: background-color 0.3s;

    &:hover {
        background-color:rgb(139 92 246);
        border:rgb(126 58 242) solid 2px
    }
`;

const ActionButton = styled(MainButton)`
    width: 50px;
    height: 50px;
    background-color: #03dac6;

    &:hover {
        background-color: #018786;
    }
`;

export default FloatingButton;
