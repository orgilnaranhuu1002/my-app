import "@/app/globals.css";
import { Spinner } from "flowbite-react";

export default function Loader() {
  return (
    
    <div  className="fixed left-0 top-0  z-50 w-full h-screen align-middle bg-blue-50">
    <div className="flex justify-center flex-col w-full h-full gap-10 items-center">
    
      <div className="flex justify-center w-[140px] h-[140px] shadow-md gap-3 shadow-gray-700  items-center  bg-blue-400 rounded-full border-blue-700  border-solid border-[4px]">
        <h1 className="flex justify-center w-full h-full text-[70px] text-white font-semibold items-center text-blue-white p-2 rounded-full">
          DO
        </h1>
      </div>
    <Spinner  className=" fill-blue-600"  size="xl" />

    </div>
  </div>
  
  );
  
}

