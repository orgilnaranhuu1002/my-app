import { createContext, useState, useEffect } from "react";
import { useDisclosure } from "@nextui-org/react";
import * as Yup from "yup";
import axios from "axios";

interface Topic {
    userId: string;
    id: string;
    title: string;
    type: string;
    description: string;
}
interface Column {
    userId: string;
    id: string;
    name: string;

}
interface MidContext {
    topics: Topic[];
    topic: { userId: string; id: string; title: string; type: string; description: string };
    setTopics: (topics: { userId: string; id: string; title: string; type: string; description: string }[]) => void;
    setTitle: (title: string) => void;
    setDescription: (description: string) => void;
    setType: (type: string) => void;
    handleAddTodo: (editedBlog: { userId: string; id: string; title: string; type: string; description: string }) => void;
    handleRemoveTodo: (id: string) => void;
    handleRemoveColumn: (columnId: string , passwords: any) => void;
    handleAddColumn: (columns: { userId: string; id: string; name: string; }) => void;
    description: string;
    type: string;
    title: string;
    userId: string;
    id: string;
    SignupSchema: Yup.ObjectSchema<any>;
    editedBlog: { userId: string; id: string; title: string; type: string; description: string };
    isOpen: any;
    setEditedBlog: (editedBlog: { userId: string; id: string; title: string; type: string; description: string } | null) => void;
    handleBlogClick: (index: number) => void;
    handleColumnClick: (index: number) => void;
    selectedBlogIndex: any;
    selectedColumnIndex: any;
    onOpenChange: (isOpen: boolean) => void;
    onClose: any;
    handleRemoveAndClose: any;
    handleDragEnd: (result: any) => void;
    getBackgroundColor: (title: string) => void;
    onCloseModal: (openModal: any) => void;
    openModal: boolean;
    setOpenModal: any;
    handleEditTodo: (editedBlog: { userId: string; id: string; title: string; type: string; description: string }) => void;
    loading: boolean;
    setLoading: (loading: boolean) => void;
    columns: Column[];
    setColumns: (columns: Column[]) => void;
    name: string;
    setName: (name: string) => void;
    column: Column;

}

const Context = createContext({} as MidContext);

const ContextProvider = (_props: { children: React.ReactChild }) => {
    const [topics, setTopics] = useState<Topic[]>([]);
    const [columns, setColumns] = useState<Column[]>([]);
    const [name, setName] = useState('');
    const [openModal, setOpenModal] = useState(false);
    const [title, setTitle] = useState('');
    const [type, setType] = useState('');
    const [description, setDescription] = useState('');
    const { isOpen, onOpen, onOpenChange, onClose } = useDisclosure();
    const [editedBlog, setEditedBlog] = useState<any | null>(null);
    const [selectedBlogIndex, setSelectedBlogIndex] = useState<number | null>(null);
    const [selectedColumnIndex, setSelectedColumnIndex] = useState<number | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const SignupSchema = Yup.object().shape({
        type: Yup.string().required('Choose a type'),
        title: Yup.string().required('Choose a title'),
        description: Yup.string().required('Please write your description'),
    });

    const topic: Topic = {
        userId: '',
        id: '',
        title: title,
        type: type,
        description: description
    };
    const column: Column = {
        userId: '',
        id: '',
        name: name,
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                const user = localStorage.getItem('User');
                const userId = user ? JSON.parse(user)._id : null;
                const response = await axios.get('/api/topics', { params: { userId } });
                console.log('Fetched topics:', response.data.topics);
                const fetchedTopics = response.data.topics.map((topic: { _id: string; title: string; type: string; description: string }) => ({
                    id: topic._id,
                    title: topic.title,
                    type: topic.type,
                    description: topic.description,
                }));
                setTopics(fetchedTopics);
                const columnsResponse = await axios.get('/api/columns', { params: { userId } });
                const fetchedColumns = columnsResponse.data.columns.map((column: { userId: string; _id: string; name: string; }) => ({
                    id: column._id,
                    name: column.name,
                }))
                setColumns(fetchedColumns);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching topics:', error);
            }
        };
        fetchData();
    }, []);
    const handleAddColumn = async (column: { userId: string; id: string; name: string; }) => {
        setLoading(true);
        try {
            const user = localStorage.getItem('User');
            const userId = user ? JSON.parse(user)._id : null;

            const response = await axios.post('/api/columns', { ...column, userId });
            const createdColumn = response.data.column
            setColumns(prevColumns => [...prevColumns, { ...createdColumn, id: createdColumn._id }]);
            setName('');
        } catch (error) {
            console.error('Error adding column:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleAddTodo = async (editedBlog: { userId: string; id: string; title: string; type: string; description: string }) => {
        try {
            setLoading(true);
            const user = localStorage.getItem('User');
            const userId = user ? JSON.parse(user)._id : null;
            const response = await axios.post('/api/topics', { ...editedBlog, userId });
            const createdTopic = response.data.topic;
            setTopics(prevTopics => [...prevTopics, { ...createdTopic, id: createdTopic._id }]);
            setDescription('');
            setType('');
            setTitle('');

            console.log("USERID IS:", userId)

        } catch (error) {
            console.error('Error adding or updating todo:', error);
        } finally {
            setLoading(false)
        }
    };

    const handleRemoveTodo = async (id: any) => {
        if (!id) {
            console.error('Error: ID is empty.');
            return;
        }

        try {
            console.log(`Deleting topic with id: ${id}`);
            await axios.delete(`/api/topics/${id}`);
            setTopics(prevTopics => prevTopics.filter(topic => topic.id !== id));
        } catch (error) {
            console.error('Error deleting topic:', error);
        }
    };
    const handleRemoveColumn = async (columnId: string, password: string) => {
        try {
            const user = localStorage.getItem('User');
            const userId = user ? JSON.parse(user)._id : null;
            const response = await axios.post('/api/columns/[id]', {
                userId: userId,
                password: password
            });
    
            if (response.status === 200) {
                const deleteResponse = await axios.delete(`/api/columns/${encodeURIComponent(columnId)}`);
                if (deleteResponse.status === 200) {
                    console.log(deleteResponse.data.message);
                    setColumns((prevColumns) => prevColumns.filter(column => column.id !== columnId));
                } else {
                    throw new Error(`Failed to delete column with status code: ${deleteResponse.status}`);
                }
            } else {
                throw new Error(`Password verification failed with status code: ${response.status}`);
            }
        } catch (error) {
            console.error('Error deleting column and topics:', error);
        }
    };
    
    
    
    const handleRemoveAndClose = () => {
        if (editedBlog && editedBlog.id) {
            handleRemoveTodo(editedBlog.id);
            setSelectedBlogIndex(null);
            onClose();
        }
    };

    const handleBlogClick = (index: number) => {
        const topic = topics[index];
        setSelectedBlogIndex(index);
        setEditedBlog(topic);
        onOpen();
    };
    const handleColumnClick = (index: number) => {    
        setSelectedColumnIndex(index);
    };

    const handleEditTodo = async (editedBlog: { userId: string; id: string; title: string; type: string; description: string }) => {
        try {
            setLoading(true);
            const user = localStorage.getItem('User');
            const userId = user ? JSON.parse(user)._id : null;
            const response = await axios.put(`/api/topics/${editedBlog.id}`, { ...editedBlog, userId });
            console.log('Edit todo response:', response.data);
            const updatedTopic = response.data.topic;
            setTopics(prevTopics =>
                prevTopics.map(topic => (topic.id === editedBlog.id ? { ...topic, ...editedBlog } : topic))
            );
            setEditedBlog(null);
            setSelectedBlogIndex(null);
            onClose();
        } catch (error) {
            console.error('Error editing todo:', error);
        } finally {
            setLoading(false)
        }
    };


    const handleDragEnd = async (result: any) => {
        try {
            if (!result.destination) {
                return;
            }
            const sourceIndex = result.source.index;
            const destinationIndex = result.destination.index;
            const draggedBlog = topics[sourceIndex];
            const destinationType = result.destination.droppableId;
            console.log('dragged')


            if (result.source.droppableId === result.destination.droppableId) {
                const updatedTopics = [...topics];
                updatedTopics.splice(sourceIndex, 1);
                updatedTopics.splice(destinationIndex, 0, draggedBlog);
                setTopics(updatedTopics)

            } else {
                const updatedBlog = { ...draggedBlog, type: destinationType };

                const response = await axios.put(`/api/topics/${draggedBlog.id}`, {
                    type: destinationType,
                    title: draggedBlog.title,
                    description: draggedBlog.description
                });

                if (response.status === 200) {
                    const updatedTopics = topics.filter((_, index) => index !== sourceIndex);
                    setTopics([...updatedTopics.slice(0, destinationIndex), updatedBlog, ...updatedTopics.slice(destinationIndex)]);
                } else {
                    throw new Error('Failed to update the blog type');
                }
            }
        } catch (error) {
            console.error("Error handling drag and drop:", error);
        }
    };

    const getBackgroundColor = (title: string) => {
        switch (title) {
            case 'Banner':
                return 'red-100';
            case 'Video':
                return 'blue-100';
            case 'Illustration':
                return 'yellow-100';
            case 'UI UX':
                return 'cyan-100';
            default:
                return 'green-100';
        }
    };

    const onCloseModal = () => {
        setOpenModal(false);
        setDescription('');
        setTitle('');
    };

    return (
        <Context.Provider
            value={{
                column,
                name,
                setName,
                handleAddColumn,
                columns,
                setColumns,
                loading,
                setLoading,
                topics,
                topic,
                openModal,
                setOpenModal,
                onCloseModal,
                getBackgroundColor,
                setEditedBlog,
                editedBlog,
                isOpen,
                selectedBlogIndex,
                selectedColumnIndex,
                onOpenChange,
                onClose,
                handleRemoveAndClose,
                SignupSchema,
                title,
                setTitle,
                setTopics,
                type,
                description,
                setDescription,
                setType,
                handleAddTodo,
                handleRemoveTodo,
                handleRemoveColumn,
                handleBlogClick,
                handleColumnClick,
                handleDragEnd,
                handleEditTodo,
                userId:'',
                id:''
            }}
        >
            {_props?.children}
        </Context.Provider>
    );
};

const ContextConsumer = Context.Consumer;

export { ContextConsumer, ContextProvider, Context };
