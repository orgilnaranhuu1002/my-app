'use client'
import React, { useEffect, useState } from 'react'
import Footer from '../components/Footer/Footer'
import { IoIosArrowBack } from 'react-icons/io';
import Image from 'next/image';
import emptyprofile from '../public/src/userProfilePic.jpeg'
import { Modal, TextInput } from "flowbite-react";
import axios from 'axios';
import { toast } from 'react-toastify';
import { useRouter } from 'next/navigation';
import { Button, Form, Input } from 'antd';
import bcrypt from 'bcryptjs';
import { EyeInvisibleOutlined, EyeTwoTone, LockOutlined, UserOutlined } from '@ant-design/icons';
import Loader from '../components/Loader';

const Profile = () => {
    const router = useRouter();
    const [showConfirmModal, setShowConfirmModal] = useState(false);
    const [loading, setLoading] = useState<boolean>(false);

    const validateMessages = {
        required: '${label} is required!',
        types: {
            email: '${label} is not a valid email!',
        },
    };



    const [userInfo, setUserInfo] = useState<{
        username: string;
        email: string;
        profileImage: string | null;
    }>({
        username: '',
        email: '',
        profileImage: null,
    });

    const [passwords, setPasswords] = useState({
        oldPassword: '',
        newPassword: '',
        confirmPassword: '',
    });
    const [openModal, setOpenModal] = useState({
        name: false,
        email: false,
        password: false,
        image: false,
        save: false,
        delete: false
    });
    const deleteCookie = (name: string) => {
        console.log('Deleting cookie:', name);
        document.cookie = `${name}=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT`;
    };
    const logout = () => {
        const confirmLogout = window.confirm("Are you sure you want to log out?");
        if (confirmLogout) {
            try {
                setLoading(true);
                window.localStorage.removeItem('User');
                deleteCookie('token');
                router.push('/login');
            } catch (error) {
                console.error('Error logging out:', error);
            } finally {
                setLoading(false);
            }
        } else {
            console.log('Logout cancelled by user.');
        }
    };
    useEffect(() => {
        if (typeof window !== 'undefined') {
            const userString = localStorage.getItem('User');
            const user = userString ? JSON.parse(userString) : null;
            if (user) {
                setUserInfo({
                    username: user.username,
                    email: user.email,
                    profileImage: user.profileImage,
                });
            }
        }
    }, []);

    const handleInputChange = (field: any, value: any) => {
        setUserInfo({
            ...userInfo,
            [field]: value,
        });
    };

    const handlePasswordChange = (field: any, value: any) => {
        setPasswords({
            ...passwords,
            [field]: value,
        });
    };
    const handleFileSelect = async (event: any) => {
        const file = event.target.files[0];
        const formData = new FormData();
        formData.append('profileImage', file);

        try {
            const response = await axios.post('/api/upload', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });

            setUserInfo({
                ...userInfo,
                profileImage: response.data.imageUrl,
            });

            const user = localStorage.getItem('User');
            const parsedUser = user ? JSON.parse(user) : null;
            const userId = parsedUser?._id;

            await axios.put(`/api/users/${userId}`, { profileImage: response.data.imageUrl });
            parsedUser.profileImage = response.data.imageUrl;
            localStorage.setItem('User', JSON.stringify(parsedUser));

            toast.success('Image uploaded successfully!');
        } catch (error) {
            toast.error('Failed to upload image');
        }
    };


    const onFinish = async (values: any) => {

        const storedUserJSON = localStorage.getItem('User');
        if (!storedUserJSON) {

            console.error('User data is not available in localStorage.');
            return;
        }

        try {
            const storedUser = JSON.parse(storedUserJSON);
            const submittedOldPassword = values.oldPassword;


            const isCorrectPassword = await bcrypt.compare(submittedOldPassword, storedUser.password);

            if (isCorrectPassword) {
                // If the password is correct, proceed with the flow
                console.log('Old password is correct. Proceed with updating the password.');

            } else {

                console.error('The old password you entered is incorrect.');
            }
        } catch (error) {

            console.error('An error occurred while comparing passwords:', error);
        }
    };

    const handleSaveChanges = async () => {
        if (passwords.newPassword !== passwords.confirmPassword) {
            toast.error("New passwords do not match");
            return;
        }
        try {
            const storedUser = JSON.parse(window.localStorage.getItem('User') || 'null');
            const userId = storedUser?._id;

            const updateData = { ...userInfo, newPassword: passwords.newPassword, oldPassword: passwords.oldPassword };

            const verifyPasswordResponse = await axios.post('/api/users/[id]', {
                userId: userId,
                password: passwords.oldPassword
            });

            if (verifyPasswordResponse.status === 200) {
                const response = await axios.put(`/api/users/${userId}`, updateData);
                console.log('Updated data id:', response.data._id);
                if (response.data._id) {
                    localStorage.setItem('User', JSON.stringify(response.data));
                    toast.success("Profile updated successfully");
                    onCloseModal();
                } else {
                    toast.error("Failed to update profile: No user ID returned from API.");
                }
            } else {
                toast.error("Incorrect old password");
            }
        } catch (error) {
            toast.error("Failed to update profile");
        }
    };


    const handleDeleteAccount = async () => {
        try {
            const storedUser = JSON.parse(window.localStorage.getItem('User') || 'null');
            const userId = storedUser?._id;
            await axios.delete(`/api/users/${userId}`, { data: { password: passwords.oldPassword } });
            localStorage.removeItem('User');
            toast.success("Account deleted successfully");
            router.push('/login');
        } catch (error) {
            toast.error("Failed to delete account");
        }
    };

    const onCloseModal = () => {
        setOpenModal({
            name: false,
            email: false,
            password: false,
            image: false,
            save: false,
            delete: false
        });
    };
    const handleGoBack = () => {
        const userString = localStorage.getItem('User');
        if (userString) {
            const storedUser = JSON.parse(userString);
            const userId = storedUser._id;
            if (storedUser && userId) {
                router.push(`/main/${userId}`);
                console.log("user id", userId);
            } else {
                console.error('User ID is undefined.');
                console.log("user id", userId);
            }
        }
    };

    return (
        <div className='bg-white flex-col p-10 w-full flex justify-center items-center h-full'>
            {loading && <Loader />}
            <div className='flex flex-col justify-center items-center w-fit h-full'>
                <div className='flex flex-row justify-between w-full items-center px-10 gap-3'>
                    <h1 className='text-[40px] text-gray-700'>Profile</h1>
                    <button onClick={handleGoBack} className='text-gray-500 flex flex-row justify-center items-center'>
                        <IoIosArrowBack />
                        Go back
                    </button>
                </div>

                <div className='w-fit h-fit text-gray-700 rounded-lg p-10 flex justify-center items-center bg-purple-50'>
                    <div className='flex flex-col justify-center items-center w-fit gap-5 bg-white rounded-xl h-fit p-10 shadow-md shadow-gray-300'>
                        <div className='flex flex-row justify-center items-center bg-white shadow-md shadow-gray-400 rounded-lg p-10 gap-5 '>
                            <div className='flex flex-col justify-center gap-3 bg-white items-start w-full h-full'>
                                <div className='flex flex-row justify-between min-w-[300px] max-w-[400px] items-center gap-4 w-full h-auto '>
                                    <h1 className='text-[20px] min-w-[120px] font-semibold'>User name :</h1>
                                    <div className='flex flex-row justify-between items-center w-full '>
                                        <p className='text-[20px] overflow-ellipsis'>{userInfo.username}</p>
                                        <button onClick={() => setOpenModal({ ...openModal, name: true })} className='bg-blue-500 ring-0 py-1 px-3 hover:bg-blue-600 transition-all duration-200 text-white rounded-md'>Edit</button>
                                    </div>
                                </div>
                                <div className='flex flex-row justify-between min-w-[300px] max-w-[400px] items-center gap-4 w-full h-auto '>
                                    <h1 className='text-[20px] min-w-[120px] font-semibold'>Email :</h1>
                                    <div className='flex flex-row justify-between gap-2 items-center w-full '>
                                        <p className='text-[20px] max-w-[200px] overflow-hidden overflow-ellipsis'>{userInfo.email}</p>
                                        <button onClick={() => setOpenModal({ ...openModal, email: true })} className='bg-blue-500 ring-0 py-1 px-3 hover:bg-blue-600 transition-all duration-200 text-white rounded-md'>Edit</button>
                                    </div>
                                </div>
                                <div className='flex flex-row justify-between min-w-[300px] max-w-[400px] items-center gap-4 w-full h-auto '>
                                    <h1 className='text-[20px] min-w-[120px] font-semibold'>Password :</h1>
                                    <div className='flex flex-row justify-between gap-2 items-center w-full '>
                                        <p className='text-[20px] max-w-[200px] overflow-hidden overflow-ellipsis'>••••••••</p>
                                        <button onClick={() => setOpenModal({ ...openModal, password: true })} className='bg-blue-500 ring-0 py-1 px-3 hover:bg-blue-600 transition-all duration-200 text-white rounded-md'>Edit</button>
                                    </div>
                                </div>
                            </div>
                            <div className='flex flex-col justify-center gap-3 items-center '>
                                <Image
                                    className='max-h-[50px] border-2 border-gray-500 m-2 max-w-[50px] rounded-full'
                                    src={userInfo.profileImage || emptyprofile}
                                    alt='Profile'
                                    width={100}
                                    height={100}
                                />
                                <button onClick={() => setOpenModal({ ...openModal, image: true })} className='bg-blue-500 ring-0 py-1 px-3 hover:bg-blue-600 transition-all duration-200 text-white rounded-md'>Edit</button>
                            </div>
                        </div>
                        <div className='flex flex-row w-full justify-end gap-5'>
                            <button className='bg-blue-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white' onClick={() => setOpenModal({ ...openModal, save: true })}>Save Changes</button>
                            <button onClick={() => setOpenModal({ ...openModal, delete: true })} className='bg-red-500  w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white'>Delete</button>
                            <button key="delete" className='bg-red-500  w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white' onClick={logout}>  Log out </button>
                        </div>
                    </div>
                </div>

                {/* Modals */}
                <Modal show={openModal.name} size="md" onClose={onCloseModal} popup>
                    <Modal.Header />
                    <Modal.Body>
                        <div className="flex flex-col justify-center items-start gap-5">
                            <p className='text-xl pl-2 text-gray-700'>Enter your new username*</p>
                            <TextInput
                                id="username"
                                placeholder="New Username"
                                value={userInfo.username}
                                onChange={(e) => handleInputChange('username', e.target.value)}
                                required
                                className='w-full px-2'
                            />
                            <div className='flex justify-end w-full pr-3 gap-3'>
                                <button className='bg-blue-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white' onClick={onCloseModal}>Done</button>
                                <button className='bg-red-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white' onClick={onCloseModal}>Close</button>

                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
                <Modal show={openModal.email} size="md" onClose={onCloseModal} popup>
                    <Modal.Header />
                    <Modal.Body>
                        <div className="flex flex-col justify-center items-start gap-5">
                            <p className='text-xl pl-2 text-gray-700'>Enter your new email*</p>

                            <TextInput
                                id="email"
                                placeholder="name@company.com"
                                value={userInfo.email}
                                onChange={(e) => handleInputChange('email', e.target.value)}
                                required
                                className='w-full px-2'

                            />
                            <div className='flex justify-end w-full pr-3 gap-3'>
                                <button className='bg-blue-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white' onClick={onCloseModal}>Done</button>
                                <button className='bg-red-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white' onClick={onCloseModal}>Close</button>

                            </div>                        </div>
                    </Modal.Body>
                </Modal>
                <Modal show={openModal.password} size="md" onClose={onCloseModal} popup>
                    <Modal.Header />
                    <Modal.Body>
                        <Form
                            className="flex flex-col justify-center items-start gap-5"
                            onFinish={onFinish}
                            name="passwordChangeForm"
                            style={{ maxWidth: 600 }}
                            validateMessages={validateMessages}
                        >
                            <p className='text-xl pl-2 text-gray-700'>Change your password*</p>

                            <Form.Item
                                name="oldPassword"
                                rules={[
                                ]}
                            >

                                <Input.Password
                                    className='w-[300px]'
                                    id="oldPassword"
                                    type="password"
                                    placeholder="Old Password"
                                    onChange={(e) => handlePasswordChange('oldPassword', e.target.value)}
                                    required
                                />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                    { required: true, message: 'Please input your password!' },
                                    { min: 8, message: 'Password too short' },
                                    { max: 20, message: 'Password too long' },

                                ]}
                                hasFeedback
                            >

                                <Input.Password
                                    className='w-[300px]'

                                    iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                                    id="newPassword"
                                    type="password"
                                    placeholder="New Password"
                                    onChange={(e) => handlePasswordChange('newPassword', e.target.value)}
                                    required
                                    prefix={<LockOutlined />}
                                />
                            </Form.Item>
                            <Form.Item
                                className='w-[300px]'

                                name="confirm"
                                dependencies={['password']}
                                hasFeedback
                                rules={[
                                    { required: true, message: 'Please confirm your password!' },
                                    ({ getFieldValue }) => ({
                                        validator(_, value) {
                                            if (!value || getFieldValue('password') === value) {
                                                return Promise.resolve();
                                            }
                                            return Promise.reject(new Error('The passwords do not match!'));
                                        },
                                    }),
                                ]}
                            >
                                <Input.Password
                                    iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                                    prefix={<LockOutlined />}

                                    id="confirmPassword"
                                    type="password"
                                    placeholder="Confirm New Password"
                                    onChange={(e) => handlePasswordChange('confirmPassword', e.target.value)}
                                    required
                                />

                            </Form.Item>
                            <Form.Item className='flex justify-center items-center'>
                                <div className='flex justify-end w-full pr-3 gap-3'>
                                    <Button type='primary' htmlType="submit" className='bg-blue-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white' onClick={onCloseModal}>Done</Button>
                                    <Button type='primary' className='bg-red-600 w-fit h-fit px-5 py-3 rounded-full shadow-md hover:bg-red-700 shadow-gray-400 text-white' onClick={onCloseModal}>Close</Button>

                                </div>
                            </Form.Item>
                        </Form>
                    </Modal.Body>
                </Modal>
                <Modal show={openModal.image} onClose={onCloseModal} dismissible={true}>
                    <Modal.Header>Change Profile Picture</Modal.Header>
                    <Modal.Body>
                        <input type="file" accept="image/*" className='text-gray-500' onChange={handleFileSelect} />
                    </Modal.Body>
                    <Modal.Footer>
                        <button className='bg-blue-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white' onClick={onCloseModal}>
                            Done
                        </button>
                        <button onClick={onCloseModal} className='bg-red-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white'>
                            Close
                        </button>
                    </Modal.Footer>
                </Modal>
                <Modal show={openModal.save} size="md" onClose={onCloseModal} popup>
                    <Modal.Header />
                    <Modal.Body>
                        <div className="flex flex-col justify-center items-start gap-5">

                            <p className='text-gray-700'>Confirm your password to save changes</p>
                            <TextInput
                                id="passwordConfirm"
                                type="password"
                                placeholder="Confirm Password"
                                onChange={(e) => {
                                    handlePasswordChange('oldPassword', e.target.value)

                                }
                                }
                                required
                            />
                            <div className='w-full flex justify-end'>
                                <button
                                    className='bg-blue-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white'
                                    onClick={() => {
                                        handleSaveChanges()
                                        onCloseModal()
                                    }
                                    }>Save Changes</button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
                <Modal show={openModal.delete} size="md" onClose={onCloseModal} popup>
                    <Modal.Header />
                    <Modal.Body className='text-gray-700'>
                        <div className="flex flex-col justify-center items-start gap-5">

                            <h1 className='text-xl '>Are you sure you want to delete your account?</h1>
                            <p className='text-gray-500 text-[12px]'>Please confirm your password to delete your account.</p>
                            <TextInput
                                id="deletePassword"
                                type="password"
                                placeholder="Password"
                                onChange={(e) => handlePasswordChange('oldPassword', e.target.value)}
                                required
                                className='w-full pr-2'

                            />
                            <div className='flex justify-end w-full pr-3 gap-3'>
                                <button
                                    className='bg-red-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white'
                                    onClick={handleDeleteAccount}>Delete</button>
                                <button
                                    className='bg-blue-600 w-fit h-fit px-5 py-3 rounded-full shadow-md shadow-gray-400 text-white'
                                    onClick={onCloseModal}>Cancel</button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
            <Footer />

        </div>

    );
}

export default Profile;
