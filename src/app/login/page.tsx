'use client'

import { useEffect, useState } from "react";
import LogIn from "../components/User/pages/LogIn";
import MiniLogIn from "../components/User/pages/MiniLogIn";
import { useRouter } from 'next/navigation';

const LoginPage= ()=>{
  const router = useRouter();
  const [isMobile, setIsMobile] = useState(true);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth >= 900)
    };

    handleResize(); // Check initial screen width
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  

	return(
	<>
	
		{isMobile ? <LogIn/>: <MiniLogIn />}
		
	</>
	)
}
export default LoginPage;