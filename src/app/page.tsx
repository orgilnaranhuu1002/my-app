
"use client"

import { useRouter } from 'next/navigation';
import Main from './components/User/pages/Main';
import MiniMain from './components/User/pages/MiniMain';
import { useEffect, useState } from 'react';
import { NextUIProvider } from "@nextui-org/react";
import Loader from './components/Loader';

export default function Home() {
  const [isMobile, setIsMobile] = useState(true);
  const router = useRouter();
  const [loading, setLoading] = useState<boolean>(false);



  useEffect(() => {
    const userString = window.localStorage.getItem('User');

    if (userString) {
      const storedUser = JSON.parse(userString);

      const userId = storedUser._id;

      router.push(`/main/${userId}`);
    }
  }, []);


  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth >= 900)
    };

    handleResize(); // Check initial screen width
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return <>
    <NextUIProvider>
      {loading && <Loader />}
      {isMobile ? <Main /> : <MiniMain />}
    </NextUIProvider>
  </>;
}
