'use client'

import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Image from 'next/image';
import emptyprofile from '@/app/public/src/userProfilePic.jpeg'
import { useRouter } from 'next/navigation';
import { IoIosArrowBack } from 'react-icons/io';
import Loader from '../components/Loader';
import Footer from '../components/Footer/Footer';
interface User {
    _id: string;
    username: string;
    profileImage?: string;
}

const UsersList = () => {
    const router = useRouter();
    const [users, setUsers] = useState<User[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const handleInviteFriend = async (inviteeId:any) => {
        try {
          const inviter = localStorage.getItem('User');
          if (inviter) {
            const inviterId = JSON.parse(inviter)._id;
            const response = await axios.post('/api/friends', { inviterId, inviteeId });
            console.log('Friend invited successfully:', response.data);
          } else {
            console.error('User not found in localStorage');
          }
        } catch (error) {
          console.error('Failed to invite friend:', error);
        }
    };
    
    const handleUnfriend = async (friendId: any) => {
        try {
          const user = localStorage.getItem('User');
          if (user) {
            const userId = JSON.parse(user)._id;
            const response = await axios.delete(`/api/friends/${userId}/${friendId}`);
            console.log('Friend removed successfully:', response.data);
          } else {
            console.error('User not found in localStorage');
          }
        } catch (error) {
          console.error('Failed to remove friend:', error);
        }
      };
    useEffect(() => {
        const fetchUsers = async () => {
          try {
            const response = await axios.get('/api/users/signup');
            if (Array.isArray(response.data.users)) {
              const userString = window.localStorage.getItem('User');
              if (userString) {
                const storedUser = JSON.parse(userString);
                if (storedUser && storedUser._id) {
                  setUsers(response.data.users.filter((user:any) => user._id !== storedUser._id));
                } else {
                  console.error('User ID is undefined.', storedUser);
                }
              } else {
                setUsers(response.data.users);
              }
              console.log(response.data.users);
            } else {
              console.error('Expected an array but got:', typeof response.data.users);
            }
          } catch (error) {
            console.error('Failed to fetch users', error);
          } finally {
            setLoading(false);
          }
        };
    
        fetchUsers();
      }, []);
    useEffect(() => {
        setLoading(false);
    }, []);
    const handleGoBack = () => {
        const userString = localStorage.getItem('User');
        if (userString) {
            const storedUser = JSON.parse(userString);
            const userId = storedUser._id;
            if (storedUser && userId) {
                router.push(`/main/${userId}`);
                console.log("user id", userId);
            } else {
                console.error('User ID is undefined.');
                console.log("user id", userId);
            }
        }
    };

    return (
        <div className='w-full min-h-screen bg-white flex justify-center items-center'>{loading && <Loader />}
            <div className='w-full max-w-[1600px] h-full flex flex-col justify-center items-center'>
                <div className='flex flex-row w-full justify-start  max-h-[1000px]  items-start' >
                    <div className='flex flex-col w-fit justify-center  items-center h-fit  text-black'>
                        <div className='flex h-full justify-start pl-5 w-full items-center '>
                            <h1 className='text-[30px] text-gray-600'>People</h1>
                        </div>
                        <div className='bg-purple-50 w-fit h-[500px]  min-w-[450px]  p-10 rounded-3xl'>
                            <div className='overflow-y-auto overflow-scroll w-full h-full p-4'>
                                <ul className='flex flex-col flex-wrap gap-3'>
                                    {users.map((user) => (
                                        <li key={user._id}>
                                            <button className='flex gap-10 text-gray-700 font-semi bg-white justify-between items-center flex-row rounded-full p-2 shadow-gray-400 shadow-md  w-[350px]  h-fit min-h-[60px]'>
                                                <div className='flex flex-row justify-center items-center'>
                                                    <Image
                                                        className=' h-[40px] bordered border-2 border-gray-500 m-2 max-w-[40px] rounded-full'
                                                        src={user.profileImage || emptyprofile}
                                                        alt='Profile'
                                                        width={100}
                                                        height={100}
                                                    />
                                                    <p className='text-[20px] max-w-[100px] overflow-hidden overflow-ellipsis '>
                                                        {user.username}
                                                    </p>
                                                </div>
                                                <button className='bg-blue-600 mr-2 ring-0 py-2 px-3 hover:bg-blue-500 transition-all duration-200 text-white rounded-full'>
                                                    Invite Friend
                                                </button>
                                            </button>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className='flex flex-col w-full justify-start items-start h-fit '>
                        <div className='flex flex-row text-gray-700 w-full px-10 justify-between items-center'>
                            <h1 className='text-[30px] text-gray-600'>Thier profile</h1>
                            <button onClick={handleGoBack} className='text-gray-500 flex flex-row justify-center items-center'>
                                <IoIosArrowBack />
                                Go back
                            </button>
                        </div>
                        <div className='w-full h-[500px] bg-blue-50 rounded-lg '>
                            {/* <div>
                            <Image
                                className=' h-[150px] bordered border-2 border-gray-500 m-2 max-w-[150px] rounded-full'
                                src={ emptyprofile}
                                alt='Profile'
                                width={100}
                                height={100}
                            />
                        </div> */}
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        </div>
    );
};

export default UsersList;