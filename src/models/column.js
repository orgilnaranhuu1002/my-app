const mongoose = require('mongoose');

const columnSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true,
        ref: 'User' 
      },
  name: {
    type: String,
    required: true
  },
  topics: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Topic' }]
});

export default mongoose.models.Column || mongoose.model("Column",columnSchema);