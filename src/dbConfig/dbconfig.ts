import mongoose from 'mongoose';

export async function connectToMongoDB() {
	if (mongoose.connection.readyState >= 1) {
        return;
    }
	try {
		mongoose.connect(process.env.MONGODB_URI!);
		const connection = mongoose.connection;

		connection.on('connected', () => {
			console.log('Great! MongoDb is connected bro!');
		});

		connection.on('error', (err) => {
			console.log('MongoDB connected ERROR. ' + err);
			process.exit();
		});
	} catch (error) {
		console.log('Ups! Something went wrong! ' + error);
	}
}
